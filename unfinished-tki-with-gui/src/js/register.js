$( document ).ready(function() {
	$( '#add-info' ).click(function() {
		let pieceOfInfo = "<label for='input-other-info' class='sr-only'>Another piece of information</label>\n \
		<input type='text' class='form-control input-other-info' placeholder='e.g. Website: example.com'>"
		$( '#other-info' ).append(pieceOfInfo);
	})
})