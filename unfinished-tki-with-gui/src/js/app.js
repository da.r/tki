App = {
	web3Provider: null,
	contracts: {},
	
	initWeb3: async function() {
		// Modern dapp browsers
		if (window.ethereum) {
			App.web3Provider = window.ethereum;
			try {
				// Request account access
				await window.ethereum.enable();
			} catch (err) {
				console.error('User denied account access');
			}
		} 
		// Legacy dapp browsers
		else if (window.web3) {
			App.web3Provider = window.web3.currentProvider;
		} 
		// Fallback to ganache if no injected web3 instace detecte (shouldn't be used in production)
		else {
			App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
		}

		web3 = new Web3(App.web3Provider);
		return App.initContract();
	},

	initContract: function() {

		$.getJSON('Tki.json', function(data) {
			// Contract artifact file instantiated with truffle-contract
			let TkiArtifact = data;
			App.contracts.Tki = TruffleContract(TkiArtifact);

			// Provider for the contract
			App.contracts.Tki.setProvider(App.web3Provider);
		})

	},

	loadFromContract: function() {
		let tkiInstance;

		App.contracts.Tki.deployed().then(function(instance) {
			tkiInstance = instance;

			tkiInstance.register(['name: john']);
			
		})
	},

}
