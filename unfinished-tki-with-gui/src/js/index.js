'use strict';

// $(document).ready(function() {
// 	$('.container').load('profile.html');
// })

let activeNavbar = 'text-light bg-secondary';


function onSelectedNavLink(sectionId) {
	// reset other links
	$( '.nav-link' ).attr('class', 'nav-link');
	//update the selected link
	$( '#nav-' + sectionId ).attr('class', 'nav-link text-light bg-secondary');
	// hide all sections
	$( '.container > div:not(#footer)').hide();
	// show only selected section
	$( '#' + sectionId ).show();
}


$( document ).ready(function() {

	App.initWeb3();
	web3.eth.getCoinbase(function(error, account) {
		if (error === null) {
			$( '#account' ).html(account);
		}
	})

	$( '#nav-profile' ).click(function() {
		onSelectedNavLink('profile');
	});

	$( '#nav-key-bindings' ).click(function() {
		onSelectedNavLink('key-bindings');
	});

	$( '#nav-users' ).click(function() {
		onSelectedNavLink('users');
	});

	$( '#nav-stats' ).click(function() {
		onSelectedNavLink('stats');
	});

});