pragma solidity >=0.4.19 <0.6.0;

// SafeMath, included from
// https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/math/SafeMath.sol
/**
 * @title SafeMath
 * @dev Unsigned math operations with safety checks that revert on error.
 */
library SafeMath {
    /**
     * @dev Multiplies two unsigned integers, reverts on overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Integer division of two unsigned integers truncating the quotient, reverts on division by zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b > 0, "SafeMath: division by zero");
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Subtracts two unsigned integers, reverts on overflow (i.e. if subtrahend is greater than minuend).
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, "SafeMath: subtraction overflow");
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Adds two unsigned integers, reverts on overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Divides two unsigned integers and returns the remainder (unsigned integer modulo),
     * reverts when dividing by zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, "SafeMath: modulo by zero");
        return a % b;
    }
}


/// @title TKI- Transparent PKI system
contract Tki {


    // parameters of the system
    uint POSTER_REWARD_RATIO = 10;
    uint POST_PRICE = 1000000;



    using SafeMath for uint;

    struct User {

        bytes32 name;
        bytes32[] info;
        
        address[] trustedBy;

        // speed up access to elements of trust array
        mapping (address => uint) trustedUsers;
        Trust[] trusts;
              
        bool registered;
    }
    
    struct Trust {
        address user;
        uint committed;
        // Could imitate trust levels from PGP: 
        //    0 -- UNKNOWN
        //    1 -- NONE
        //    2 -- MARGINAL
        //  >=3 -- FULL
        uint level;
    }
    
    struct Claim {
        address claimant;
        bytes32 token;

        // Claimant can specify a reward
        Reward reward;

        // one user, one verification
        mapping (address => bool) verified;
    }
    
    struct KeyBinding {
        address poster;

        // Speed up finding a claim
        mapping (address => uint) claimants;
        Claim[] claims;
        
        mapping (address => uint) numVerifications;

        uint highestNumVerifications;
        address strongestClaimant;

        // one user, one visit
        mapping (address => bool) visited;
        uint numVisits;

        // is the binding declared revoked by the strongest claimant?
        bool revoked;

        mapping (address => uint) rewardIssuers;
        Reward[] rewards;

        // Could be an array, so that several certificates can be associated
        //  with the binding
        string certificateHash;
    }

    // Poster can also offer reward because she might have an interest in having
    // the website verified
    struct Reward {
        address issuer;
        uint balance;
        uint visitReward;
        // relates to the domain
        uint verificationReward;

        bool visitRewardInactive;
        bool verifRewardInactive;
    }
        
    mapping (address => User) public users;
    
    // domain names are used as keys to the outer mapping
    // sha3-hashes of public keys are used as keys to the inner mapping
    mapping (bytes32 => mapping (bytes32 => KeyBinding)) public bindings;

    // Fund for posting rewards, not related to the reward structures defined
    // by individual TKI users and associated with key bindings
    uint postRewardFund = 0;
    

    event NewUser(address indexed user, bytes32 indexed name);
    event UserUpdatedInfo(address indexed user, bytes32 indexed name, bytes32[] info);
    event AddedToPostFund(address indexed user, uint amount);
    event NewBindingPosted(address indexed poster, bytes32 indexed domain, bytes32 indexed kecPubKey,
            uint visitReward, uint verificationReward);
    event CreatedBindingReward(address indexed rewardIssuer, bytes32 indexed domain, bytes32 indexed kecPubKey, 
            uint visitReward, uint verificationReward);
    event UpdatedBindingReward(address indexed rewardIssuer, bytes32 indexed domain, bytes32 indexed kecPubKey, 
            uint visitReward, uint verificationReward);
    event ClaimedKeyBinding(address indexed claimant, bytes32 indexed domain, bytes32 indexed kecPubKey, bytes32 token,
            uint visitReward, uint verificationReward);
    event UpdatedClaim(address indexed claimant, bytes32 indexed domain, bytes32 indexed kecPubKey, bytes32 token, 
            uint visitReward, uint verificationReward);
    event InsufficientFunds(address indexed rewardIssuer, bytes32 indexed domain, bytes32 indexed kecPubKey);
    event BindingRevoked(address indexed user, bytes32 indexed domain, bytes32 indexed kecPubKey);
    event UserVisitedBinding(address indexed user, bytes32 indexed domain, bytes32 indexed kecPubKey);
    // Client that verified the claim is logged as the message sender
    event VerifiedClaim(address indexed claimant, bytes32 indexed domain, bytes32 indexed kecPubKey);
    event InitializedTrust(address indexed sender, address indexed receiver, uint indexed level, uint amount);
    event UpdatedTrust(address indexed sender, address indexed receiver, uint indexed level, uint amount);


    /**
     * Registers a user in TKI. Registered users can access functions of TKI
     * system. It makes extension of the system possible. For example, one could
     * include a mechanism where a certain number of existing TKI users have to
     * decide on whether to accept someone's registration.
     * Emits NewUser event.
     * 
     * @param  name    name of the registrant 
     * @param  info    array of information pieces
     */
    function register(bytes32 name, bytes32[] memory info) public {
        require(!users[msg.sender].registered);
        
        users[msg.sender].name = name;
        users[msg.sender].info = info;
        users[msg.sender].registered = true;

        emit NewUser(msg.sender, name);
    }


    /**
     * Sets the user info (TKI user profile) of the sender to the given name
     * and info array.
     * Emits UserUpdatedInfo event.
     * 
     * @param  name    new name to be associated with an ethereum account
     * @param  info    new pieces of information
     */
    function updateUserInfo(bytes32 name, bytes32[] memory info) public {
        require(users[msg.sender].registered);
        users[msg.sender].name = name;
        users[msg.sender].info = info;
        
        emit UserUpdatedInfo(msg.sender, name, info);
    }

    /**
     * Ether sent with the transaction is added to the contract balance that is 
     * used to reward users which post key bindings to the blockchain. 
     * Emits AddedToPostFund event.
     * Unregistered accounts can also send ether.
     */
    function addToPostFund() public payable {
        postRewardFund = postRewardFund.add(msg.value);
        emit AddedToPostFund(msg.sender, msg.value);
    }
    

    /**
     * Posts the given key binding to the blockchain. Creates a reward with the
     * specified visit reward and links key binding to the hash of the 
     * certificate stored on IPFS.
     * Poster should include amount of ether higher than defined in POST_PRICE.
     * Sent ether is added to the reward balance.
     * Public key is not a parameter, domain name is instead linked to its hash 
     * calculated using sha3.
     * Emits NewBindingPosted and UserVisitedBinding events.
     * 
     * @param  domainName               domain in the key binding
     * @param  kecPubKey                hash of the public key
     * @param  visitReward              compensation that the visitors get when
     *                                               they visit the website
     * @param  verificationReward       compensation that users get when they
     *                                               verify the domain
     * @param  certificateHash    hash of the contract on IPFS
     */
    function postKeyBinding(bytes32 domainName, bytes32 kecPubKey,  uint visitReward, uint verificationReward, string memory certificateHash) public payable {
        require(users[msg.sender].registered);

        // check binding for existence
        require(bindings[domainName][kecPubKey].poster == address(0x0));

        // to protect against spamming of false key bindings
        require(msg.value >= POST_PRICE);

        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        curBinding.poster = msg.sender;

        curBinding.visited[msg.sender] = true;
        curBinding.numVisits = curBinding.numVisits.add(1);

        curBinding.rewards.push(Reward({
            issuer: msg.sender,
            balance: msg.value,
            visitReward: visitReward,
            verificationReward: verificationReward,
            visitRewardInactive: false,
            verifRewardInactive: false
        }));

        // Length instead of .length - 1 is stored so that index 0 can represent
        // "no reward bound to the account"
        curBinding.rewardIssuers[msg.sender] = curBinding.rewards.length;

        curBinding.certificateHash = certificateHash;
        
        emit NewBindingPosted(msg.sender, domainName, kecPubKey, visitReward, verificationReward);
        emit UserVisitedBinding(msg.sender, domainName, kecPubKey);

        // incentivize users to post bindings
        uint toTransfer = postRewardFund.div(POSTER_REWARD_RATIO);
        postRewardFund = postRewardFund.sub(toTransfer);
        msg.sender.transfer(toTransfer);
    }


    /**
     * Create a reward with the given visit and verification compensations linked
     * to the key binding with the specified domain and public key hash.
     * Created reward is associated with the sender and the specified key binding.
     * Ether sent with the transaction is added to the reward balance.
     * Emits CreatedBindingReward event.
     * 
     * @param  domainName         domain in the key binding
     * @param  kecPubKey          public key hash in the key binding
     * @param  visitReward        reward that visitors get when they visit the website
     * @param  verificationReward reward that users get when they verify the domain
     */
    function createBindingReward(bytes32 domainName, bytes32 kecPubKey, uint visitReward, uint verificationReward) public payable {
        require(users[msg.sender].registered);

        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        // there should be no reward associated with the caller
        require(curBinding.rewardIssuers[msg.sender] == 0);

        curBinding.rewards.push(Reward({
            issuer: msg.sender,
            balance: msg.value,
            visitReward: visitReward,
            verificationReward: verificationReward,
            visitRewardInactive: false,
            verifRewardInactive: false
        }));

        curBinding.rewardIssuers[msg.sender] = curBinding.rewards.length;

        emit CreatedBindingReward(msg.sender, domainName, kecPubKey, visitReward, verificationReward);
    }


    /**
     * Updates the reward associated with the specified key binding and the sender.
     * Ether sent with the transaction is added to the reward balance.
     * Emits UpdatedBindingReward event.
     * 
     * @param  domainName         domain in the key binding
     * @param  kecPubKey          public key hash in the key binding
     * @param  visitReward        reward that the visitors get when they visit the website
     * @param  verificationReward reward that users get when they verify the domain
     */
    function updateBindingReward(bytes32 domainName, bytes32 kecPubKey, uint visitReward, uint verificationReward) public payable {
        require(users[msg.sender].registered);

        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        // reward associated with the caller should already exist
        require(curBinding.rewardIssuers[msg.sender] != 0);

        uint id = curBinding.rewardIssuers[msg.sender].sub(1);

        curBinding.rewards[id].balance = curBinding.rewards[id].balance.add(msg.value);
        curBinding.rewards[id].visitReward = visitReward;
        curBinding.rewards[id].verificationReward = verificationReward;

        if (curBinding.rewards[id].balance >= curBinding.rewards[id].visitReward) {
            curBinding.rewards[id].visitRewardInactive = false;
        }

        if (curBinding.rewards[id].balance >= curBinding.rewards[id].verificationReward) {
                curBinding.rewards[id].verifRewardInactive = false;
        }
        
        emit UpdatedBindingReward(msg.sender, domainName, kecPubKey, visitReward, verificationReward);
    }


    /**
     * Sender claims that she is the owner of the specified key binding.
     * It creates reward associated with the specified key binding and the sender.
     * Ether sent with the transaction is serves as initial balance of created
     * reward.
     * Emits ClaimedKeyBinding event.
     * 
     * @param  domainName         domain in the key binding
     * @param  kecPubKey          public key hash in the key binding
     * @param  token              token included in token object that is put on 
     * the website and used in verification
     * @param  visitReward        reward that the visitors get when they visit the
     * website; 
     * @param  verificationReward reward that users get when they verify
     * the domain; linked to the sender and the claimed key binding
     */
    function claimKeyBinding(bytes32 domainName, bytes32 kecPubKey, bytes32 token, uint visitReward, uint verificationReward) public payable {
        require(users[msg.sender].registered);
        
        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        // is the binding posted?
        require(curBinding.poster != address(0x0));
        // there should be no claim associated with the caller
        require(curBinding.claimants[msg.sender] == 0);

        curBinding.claims.push(Claim({
            claimant: msg.sender,
            token: token,
            reward: Reward({
                issuer: msg.sender,
                balance: msg.value,
                visitReward: visitReward,
                verificationReward: verificationReward,
                visitRewardInactive: false,
                verifRewardInactive: false
            })
        }));

        curBinding.claimants[msg.sender] = curBinding.claims.length;

        emit ClaimedKeyBinding(msg.sender, domainName, kecPubKey, token, visitReward, verificationReward);
    }


    /**
     * Updates the claim the sender made to the specified key binding. 
     * It updates the reward associated with the specified key binding and the sender.
     * Ether sent with the transaction is added to created reward balance.
     * Emits UpdatedClaim event.
     * 
     * @param  domainName         domain in the key binding
     * @param  kecPubKey          public key hash in the key binding
     * @param  token              token included in token object that is put on 
     * the website and used in verification
     * @param  visitReward        reward that the visitors get when they 
     * visit the website
     * @param  verificationReward reward that users get when they verify
     * the domain; linked to the sender and the claimed key binding
     */
    function updateClaim(bytes32 domainName, bytes32 kecPubKey, bytes32 token, uint visitReward, uint verificationReward) public payable {
        require(users[msg.sender].registered);

        KeyBinding storage curBinding =  bindings[domainName][kecPubKey];

        // restrict it to claimant
        // claim associated with the caller should exist
        require(curBinding.claimants[msg.sender] != 0);

        uint id = curBinding.claimants[msg.sender].sub(1);

        // token object on the website should be signed with claimant's private key 
        curBinding.claims[id].token = token;
        curBinding.claims[id].reward.balance = curBinding.claims[id].reward.balance.add(msg.value);
        curBinding.claims[id].reward.visitReward = visitReward;
        curBinding.claims[id].reward.verificationReward = verificationReward;

        if (curBinding.claims[id].reward.balance >= curBinding.claims[id].reward.visitReward) { 
            curBinding.claims[id].reward.visitRewardInactive = false;
        }

        if (curBinding.claims[id].reward.balance >= curBinding.claims[id].reward.verificationReward) {        
                curBinding.claims[id].reward.verifRewardInactive = false;
        }

        emit UpdatedClaim(msg.sender, domainName, kecPubKey, token, visitReward, verificationReward);
    }


    /**
     * Notes that the sender visited the specified key binding.
     * Emits UserVisitedBinding event.
     * 
     * @param  domainName    domain in the key binding
     * @param  kecPubKey     public key hash in the key binding
     */
    function visitedKeyBinding(bytes32 domainName, bytes32 kecPubKey) public {
        require(users[msg.sender].registered);

        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        // one user, one visit
        require(!curBinding.visited[msg.sender]);
        curBinding.visited[msg.sender] = true;
        
        curBinding.numVisits = curBinding.numVisits.add(1);

        uint toTransfer = 0;
        
        // reward the visitor
        for (uint i = 0; i < curBinding.rewards.length; ++i) {
            if (curBinding.rewards[i].balance >= curBinding.rewards[i].visitReward && !curBinding.rewards[i].visitRewardInactive) {
                toTransfer = toTransfer.add(curBinding.rewards[i].visitReward);
                curBinding.rewards[i].balance = curBinding.rewards[i].balance.sub(curBinding.rewards[i].visitReward);
            }
            // otherwise this visit reward is not counted to the sum of all visit rewards 
            // because it cannot be paid out
            else if (!curBinding.rewards[i].visitRewardInactive) {
                curBinding.rewards[i].visitRewardInactive = true;
                emit InsufficientFunds(curBinding.rewards[i].issuer, domainName, kecPubKey);
            }
        }

        // transfer visit rewards from claims
        for (uint j = 0; j < curBinding.claims.length; ++j) {
            Reward storage curReward = curBinding.claims[j].reward;
            if (curReward.balance >= curReward.visitReward && !curReward.visitRewardInactive) {
                toTransfer = toTransfer.add(curReward.visitReward);
                curReward.balance = curReward.balance.sub(curReward.visitReward);
            } else if (!curReward.visitRewardInactive) {
                curReward.visitRewardInactive = true;
                emit InsufficientFunds(curReward.issuer, domainName, kecPubKey);
            }
        }
        
        emit UserVisitedBinding(msg.sender, domainName, kecPubKey);

        msg.sender.transfer(toTransfer);
    }

   
    /**
     * Notes that the sender verified the specified key binding of the claimant
     * Ether sent with the transaction is added to the contract balance that is 
     * used to reward users which post key bindings to the blockchain.
     * 
     * @param  domainName    domain in the key binding
     * @param  kecPubKey     public key hash in the key binding
     * @param  claimant      address of the claimant
     */
    function verifiedClaim(bytes32 domainName, bytes32 kecPubKey, address claimant) public {
        require(users[msg.sender].registered);
        
        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        // check binding for existence -- binding has to be posted before it 
        // can be verified
        require(curBinding.poster != address(0x0));

        uint id = curBinding.claimants[claimant].sub(1);
        require(!curBinding.claims[id].verified[msg.sender]);
        
        uint toTransfer = 0;
        
        // Reward the verifier with rewards of every claimant
        for (uint i = 0; i < curBinding.claims.length; ++i) {
            Reward storage curReward = curBinding.claims[i].reward;
            if (curReward.balance >= curReward.verificationReward && !curReward.verifRewardInactive) {
                toTransfer = toTransfer.add(curReward.verificationReward);
                curReward.balance = curReward.balance.sub(curReward.verificationReward);
            } else if (!curReward.verifRewardInactive) {
                curReward.verifRewardInactive = true;
                emit InsufficientFunds(curReward.issuer, domainName, kecPubKey);
            }
        }

        // check whether the claim exists
        // uint initialized by default to 0 in mapping, so check the claimant's address too 
        if ((id <= curBinding.claims.length) && (curBinding.claims[id].claimant != msg.sender)) {
            curBinding.numVerifications[claimant] = curBinding.numVerifications[claimant].add(1);
            curBinding.claims[id].verified[msg.sender] = true;
            emit VerifiedClaim(claimant, domainName, kecPubKey);
        }

        // set highestNumVerifications to id of the claimant with the most
        // verifications
        if (curBinding.numVerifications[claimant] >= curBinding.highestNumVerifications) {
            curBinding.highestNumVerifications = curBinding.numVerifications[claimant];
            curBinding.strongestClaimant = claimant;
        }

        msg.sender.transfer(toTransfer);
      }
    

    /**
     * If sender is the claimant with the most verifications, given key binding
     * is declared revoked.
     * 
     * @param domainName domain in key binding
     * @param kecPubKey  hash of to the public key in key binding
     */
    function revokeKeyBinding(bytes32 domainName, bytes32 kecPubKey) public {
        require(users[msg.sender].registered);


        // check binding for existence
        require(bindings[domainName][kecPubKey].poster != address(0x0));

        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        if (curBinding.strongestClaimant == msg.sender) {
            curBinding.revoked = true;
            emit BindingRevoked(msg.sender, domainName, kecPubKey);
        }
    }


    /**
     * Notes that the sender initialized trust in specified user on the 
     * given level.
     * Ether sent with transaction is added to the contract balance that is 
     * used to reward users which post key bindings to the blockchain, and this
     * ether is used to indicate trust of the sender in the user in the argument.
     * Emits the InitializedTrust event.
     * 
     * @param  user          ethereum address of the user that
     * is trusted
     * @param  level         level of trust in the user (higher level => more trust)
     */
    function initTrust(address user, uint level) public payable {
        require(users[msg.sender].registered);
        require(users[user].registered);

        // if trust relationship already exists it cannot be initialized
        require(users[msg.sender].trustedUsers[user] == 0);

        users[msg.sender].trusts.push(Trust({
            user: user,
            committed: msg.value,
            level: level
        }));
        users[msg.sender].trustedUsers[user] = users[msg.sender].trusts.length;

        users[user].trustedBy.push(msg.sender);
        
        emit InitializedTrust(msg.sender, user, level, msg.value);
    }


    /**
     * Updates trust of the sender in the specified user with the given trust 
     * level.
     * Ether sent with transaction is added to posting reward fund and its value
     * is added up to the value that represents amount of ether sent by the 
     * sender in the name of this trust relationship.
     * Emits UpdatedTrust event.
     * 
     * @param  user          ethereum address of the user that is trusted
     * @param  level         new level of trust in the user 
     *                           (higher level => more trust)
     */
    function updateTrust(address user, uint level) public payable {
        require(users[msg.sender].registered);
        require(users[user].registered);

        uint id = users[msg.sender].trustedUsers[user].sub(1);

        Trust storage curTrust = users[msg.sender].trusts[id];
        curTrust.user = user;
        curTrust.committed = curTrust.committed.add(msg.value);
        curTrust.level = level;
        
        emit UpdatedTrust(msg.sender, user, level, msg.value);
    }


    /**
     * Getter for the claimants related to the specified key binding.
     * 
     * @param  domainName    domain in the key binding
     * @param  kecPubKey     public key hash in the key binding
     * @return {address[]}         returns an array of ethereum address claimants
     */
    function getClaimants(bytes32 domainName, bytes32 kecPubKey) public view returns (address[] memory) {
        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        address[] memory claimants = new address[](curBinding.claims.length);

        for (uint i = 0; i < curBinding.claims.length; ++i) {
            claimants[i] = curBinding.claims[i].claimant;
        }

        return claimants;
    }


    /**
     * Gets addresses of users trusted by the given TKI user with a trust level
     * higher than specified.
     *
     * @param  user             address of the user whose trusted addresses are
     *                                  listed
     * @param  level            minimal trust level of the users to get
     * @return {address[]}      returns an array of etherem addresses of the users
     * trusted by transaction's sender
     */
    function getTrustedUsers(address user, uint level) public view returns (address[] memory) {
        User memory caller =  users[user];

        address[] memory trustedUsers = new address[](caller.trusts.length);

        uint offsetUntrusted = 0;
        for (uint i = 0; i < caller.trusts.length; ++i) {
            if (level <= caller.trusts[i].level) {
                trustedUsers[i.sub(offsetUntrusted)] = caller.trusts[i].user;
            } else {
                offsetUntrusted = offsetUntrusted.add(1);
            }
        }

        return trustedUsers;
    } 


    /**
     * Returns all of the rewards associated with the given key binding.
     * Returned arrays contain specific elements of reward structures.
     * Elements in arrays on a specific index belong to the same reward structure.
     */
    function getRewards(bytes32 domainName, bytes32 kecPubKey) public view returns(address[] memory issuers, uint[] memory balances, 
                                              uint[] memory visitRewards, uint[] memory verificationRewards, 
                                              bool[] memory visitRewardInactives, bool[] memory verifRewardInactives)
    {
        KeyBinding storage curBinding = bindings[domainName][kecPubKey];

        issuers = new address[](curBinding.rewards.length.add(curBinding.claims.length));
        balances = new uint[](curBinding.rewards.length.add(curBinding.claims.length));
        visitRewards = new uint[](curBinding.rewards.length.add(curBinding.claims.length));
        verificationRewards = new uint[](curBinding.rewards.length.add(curBinding.claims.length));
        visitRewardInactives = new bool[](curBinding.rewards.length.add(curBinding.claims.length));
        verifRewardInactives = new bool[](curBinding.rewards.length.add(curBinding.claims.length));

        for (uint i = 0; i < curBinding.rewards.length; ++i) {
            issuers[i] = curBinding.rewards[i].issuer;
            balances[i] = curBinding.rewards[i].balance;
            visitRewards[i] = curBinding.rewards[i].visitReward;
            verificationRewards[i] = curBinding.rewards[i].verificationReward;
            visitRewardInactives[i] = curBinding.rewards[i].visitRewardInactive;
            verifRewardInactives[i] = curBinding.rewards[i].verifRewardInactive;
        }

        uint idx = curBinding.rewards.length;

        for (uint j = 0; j < curBinding.claims.length; ++j) {
            issuers[j.add(idx)] = curBinding.claims[j].reward.issuer;
            balances[j.add(idx)] = curBinding.claims[j].reward.balance;
            visitRewards[j.add(idx)] = curBinding.claims[j].reward.visitReward;
            verificationRewards[j.add(idx)] = curBinding.claims[j].reward.verificationReward;
            visitRewardInactives[j.add(idx)] = curBinding.claims[j].reward.visitRewardInactive;
            verifRewardInactives[j.add(idx)] = curBinding.claims[j].reward.verifRewardInactive;
        }

        return (issuers, balances, visitRewards, verificationRewards, visitRewardInactives, verifRewardInactives);
    }
}
