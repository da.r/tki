# TKI - Transparent (Public) Key Infrastructure

PKI system that extends traditional CA-based PKI model by allowing users to store information about encountered key bindings on blockchain. Prototype of TKI consists of a (1) smart contract written in Solidity, (2) Firefox browser extension, and (3) command line application. Both browser extension and command line application interact with the smart contract on blockchain through web3.js.



## Installation / Starting the system
  * [Truffle](https://truffleframework.com/) Suite and Ganache have to be installed. Truffle v5.0.2 was used during development.
  * Browser extension can be included in Firefox by navigating to `about:debugging` and clicking the button 'Load Temporary Add-on'.
  * Running `npm install` might be needed and by calling `npm link` in `TKI/tki-cli` folder one can install command `tki` defined in 'package.json' so that one does not have to call the application using (assuming one is in 'tki-cli' folder) `node bin/tki-bin.js`.
  * Linking the blockchain with command line application and browser extension:
    1. Ganache should be started and running. The 'host', 'port', and 'network_id' should correspond to those in `tki-cli/truffle-config.js`. Gas Limit, which can be set in Ganache settings: 'settings (in top right corner) > Chain', should also be set high enough for deployment of TKI contract, e.g. 5,000,000. 
	2. Deploy on Ganache private blockchain and copy contract ABI to browser extension folder using (should be called from `TKI/tki-cli` folder):
		```truffle migrate --reset && cp build/contracts/Tki.json ../tkiExtens```
	3. Tki contract address that is contained in the output of the previous command should be set as the current address in config file using:
		```tki config -s -c <contract address without '0x' prefix>```

| ![(Figure:contractAddressToCopy.png)](./contractAddressToCopy.png) |
|:---:|
| *Fig. 1. Contract address to copy in step 3* |



	
## Browser Extension
Clicking on the extension icon opens a popup that contains the following functionalities:
  * Unregistered user can register by clicking on the *Register* button.
  * All of the bindings encountered on the website are listed in the popup. It might take some time after visiting the website for all key bindings to appear. Multiple opennings of the popup might be needed. Folded key binding info card shows the following information from left to right (see Figure 2): (1) domain, (2) number of visits to the domain, (3) number of trusted visits to the domain, (4) number of times key binding was seen, (5) number of times key binding was seen by trusted users
  * When default user records a visit, webpage has to be refreshed before the visit can be seen in the popup.
  * Clicking on the domain (i.e. folded key binding info card) shows information stored on blockchain related to the encountered key binding (unfolded key binding info card), such as recorded visits (see Figure 3). Clicking on the domain again hides shown information, i.e. folds the key binding info card. 
  * If *Send new key bindings and vote for existing* checkbox is set, user will record visits on blockchain for every key binding that she has not yet visited. When user encounters a key binding that has not been posted to blockchain, *Post Bindings* button becomes available in the popup that can be opened by clicking on the extension icon. 
  * *Update Set of Trusted Users* button recalculates the set of users trusted by the default user and in the course of this takes into account all changes to the accepted trust level and length of the trust chain. Users contained in this set are considered as trusted users for the  presentation of recorded trusted visits and verifications associated with the key binding. After the set is successfully updated, color of the button changes to green for 2 seconds. Changes in recorded trusted visits and verifications are visible only after reloading the webpage.
  * In *Options* page users can specify the maximum length of the trust chain and minumum trust level considered trusted, as well as limit visits taken into account to only those after a certain block, i.e. visits and verifications that are not older than some amount of time.

| ![(Figure:foldedKeyBinding.png)](./foldedKeyBinding.png)| ![(Figure:unfoldedKeyBinding.png)](./unfoldedKeyBinding.png) |
|:---:|:---:|
| *Fig. 2. Folded key binding info card example* | *Fig. 3. Unfolded key binding info card example*
  


## Command Line Application
Command line application is a truffle project that uses [yargs](https://github.com/yargs/yargs) for parsing command line arguments. It allows interaction with the smart contract that is defined in `TKI/tki-cli/contracts/Tki.sol`.
  * `tki --help` shows descriptions and examples of all commands.
  * `tki <command> --help` lists options that can be used with the chosen command. 
  * Most of the commands have short aliases to enable easier use of the system, e.g. `tki list --bindings` can be written `tki l -b`
  * As the configuration file is addressed with relative path, `tki config` only works when called from within `TKI/tki-cli` folder. Data associated with the blockchain instance to which the application connects is defined in `tki-cli/bin/resources/config.json` file.

**Note:** When addresses are given as inputs to CLI commands they shoud not contain '0x' prefix.

  | ![(Figure:tki-cli.png)](./tki-cli.png) |
  |:---:|
  | *Fig. 4. Output of `tki --help`* |

## Notes
* [Forge](https://github.com/digitalbazaar/forge#security-considerations) is used to process certificates and obtain raw public keys. 
* Although TKI stores certificates to IPFS, raw public keys could be stored either to blockchain or IPFS as well. Drawback of this approach is that some of the certificates cannot be processed. Another approach could be to use a hash of the Subject Public Key Info component of X.509 certificates, but raw public keys would then be unavailable.
* Browser extension is made for Firefox because it is the most popular browser that allows browser extension access to certificate data.
  

