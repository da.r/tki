'use-strict';

const TkiLib = require('./lib/tki-lib');
const web3 = require('web3');
const contract =  require('truffle-contract');

const Tki = contract(require('./build/contracts/Tki.json'));

module.exports = function(config) {
	let web3Provider = new web3.providers.HttpProvider(`http:\/\/${config.host}:${config.port}`);
	// '0x' prepended because stored addresses do not contain prefixes
	return new TkiLib(Tki, '0x' + config.contractAddress, web3Provider, '0x' + config.defaultAccount, config.defaultGas); 
}