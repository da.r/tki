'use strict';


// web3 v0.18.4 is included in truffle-contract library
const Web3 = require('web3');
const IPFS = require('ipfs-mini');
const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });
const crypto = require('crypto');

const ethUtils = require('ethereumjs-util');
const request = require('request');
const BigNumber = require('bignumber.js');

module.exports = class Lib {

	/**
	 * Initializes the library. Creates an instance of Tki contract associated
	 * with a certain Ethereum address. This contract address can be found in 
	 * the output of `truffle migrate` command.
	 * @param  Tki             truffle's contract abstraction
	 * @param  contractAddress address of Tki contract 
	 * @param  web3Provider    provides web3 access to blockchain
	 * @param  defaultAccount  account used to call contract function by default
	 * @param  defaultGas      gas set by default in contract calls
	 */
	constructor(Tki, contractAddress, web3Provider, defaultAccount, defaultGas) {
		this.web3 = new Web3(web3Provider);
		this.Tki = Tki;
		this.Tki.setProvider(web3Provider);
		this.Tki.defaults({
			from: defaultAccount,
			gas: defaultGas
		});

		this.contract = Tki.at(contractAddress);
	}

	/**
	 * Registers the given Ethereum address to TKI. Pieces of information in 
	 * user info should be separated by ';'
	 * @param  address Ethereum address to register in tKI
	 * @param  name    name associated with the address
	 * @param  infos   TKI user information / profile associated with the address
	 */
	register(address, name, infos) {
		let infoArr = infos.split(';');
		// convert to bytes
		for (let i = 0; i < infoArr.length; ++i) {
			// if it is '' it converts it to '0x'
			if (infoArr[i] != '') {
				infoArr[i] = this.web3._extend.utils.fromAscii(infoArr[i]);
			}
		}
		this.contract.register(this.web3._extend.utils.fromAscii(name), infoArr, {from: address}).then(() => {
			console.log(`Registered ${address} with the following information: \n\t${infos}`);
		});
	}

	/**
	 * Updates TKI user account associated with the given Ethereum address. 
	 * Pieces of information in user info should be separated by ';'
	 * @param  address Ethereum address of the TKI user
	 * @param  name    new name to set
	 * @param  infos   new user information to associate with addresss
	 */
	updateUserInfo(address, name, infos) {
		let infoArr = infos.split(';');
		// convert to bytes
		for (let i = 0; i < infoArr.length; ++i) {
			infoArr[i] = this.web3._extend.utils.fromAscii(infoArr[i]);
		}
		this.contract.updateUserInfo(this.web3._extend.utils.fromAscii(name), infoArr, {from: address}).then(() => {
			console.log(`Updated user info of ${address} with the following information: \n${infos}`);
		})
	}

	/**
	 * Posts key binding to blockchain and associates with it a reward structure
	 * defined with the given parameters
	 * @param  domainName      domain in key binding
	 * @param  pubKeyHash      hash of the public key that is part of the binding
	 * @param  visitReward     compensation paid out on recorded visit 
	 * @param  verifReward     compensation paid out on recorded verification
	 * @param  certificateHash hash of the certificate posted to IPFS
	 * @param  forBalance      ether sent to serve as inital balance of reward
	 *                               used to pay visit and verification 
	 *                               compensations
	 */
	postKeyBinding(domainName, pubKeyHash, visitReward, verifReward, certificateHash, forBalance) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		this.contract.postKeyBinding(hexDomainName, pubKeyHash, visitReward, verifReward, certificateHash, 
			{value: forBalance}).then(() => {
				console.log(`(${domainName}, ${pubKeyHash}) key binding posted to blockhain.`
				+ ` Certificate hash: ${certificateHash}\nDefiniton of the reward associated with the binding:`
				+ `\n\tVisit reward: ${visitReward}\n\t Verification reward: ${verifReward}\n\tInitial balance: ${forBalance}`);
			});
	}

	/**
	 * Creates a reward structure that is associated with the given key binding.
	 * @param  domainName  domain in key binding
	 * @param  pubKeyHash  hash of the public key that is part of the binding
	 * @param  visitReward compensation paid out on recorded visit
	 * @param  verifReward compensation paid out on recorded verification
	 * @param  forBalance  ether sent to serve as inital balance of reward
	 *                               used to pay visit and verification 
	 *                               compensations
	 */
	createBindingReward(domainName, pubKeyHash, visitReward, verifReward, forBalance) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		this.contract.createBindingReward(hexDomainName, pubKeyHash, visitReward, verifReward, {value: forBalance}).then(() => {
			console.log(`Created a reward for (${domainName}, ${pubKeyHash}) key binding with: \n\tVisit reward: ${visitReward}`
				+ `\n\tVerification reward: ${verifReward}\n\treward balance: ${forBalance}`);
		});
	}

	/**
	 * Updates the reward structure associated with the given key binding.
	 * visitReward and verifReward are updated while ether sent for reward
	 * balance is added to the existing balance.
	 * @param  domainName  domain in key binding
	 * @param  pubKeyHash  hash of the public key that is part of the binding
	 * @param  visitReward compensation paid out on recorded visit
	 * @param  verifReward compensation paid out on recorded verification
	 * @param  forBalance  ether added to the reward balance
	 */
	updateBindingReward(domainName, pubKeyHash, visitReward, verifReward, forBalance) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		this.contract.updateBindingReward(hexDomainName, pubKeyHash, visitReward, verifReward, {value: forBalance}).then(() => {
			console.log(`Updated a reward for (${domainName}, ${pubKeyHash}) key binding with: \n\tVisit reward: ${visitReward}`
				+ `\n\tVerification reward: ${verifReward}\n\treward balance: ${forBalance}`);
		});	
	}

	/**
	 * Lists rewards associated with a key binding.
	 * @param  domainName domain in key binding
	 * @param  pubKeyHash hash of the public key that is part of the binding
	 */
	async listBindingRewards(domainName, pubKeyHash) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);

		// Arrays in the outer array are specified in smart contract function
		let rewards = await this.contract.getRewards.call(hexDomainName, pubKeyHash);
		
		for (let i = 0; i < rewards[0].length; ++i) {
			console.log(i+1 + " Creator: " + rewards[0][i] + '; Balance: ' + rewards[1][i].toString() 
				+ '; Compensation on visit: ' + rewards[2][i].toString() + '; Compensation on verification: '
				+ rewards[3][i].toString());
		}
		
	}

	/**
	 * Default user claims the key binding and associates a reward with the 
	 * claim.
	 * @param  domainName         domain in key binding
	 * @param  pubKeyHash         hash of the public key that is part of the binding
	 * @param  token              token associated with the claim put on website
	 * @param  visitReward        compensation paid out on recorded visit
	 * @param  verificationReward compensation paid out on recorded verification
	 * @param  forBalance         ether sent to serve as inital balance of reward
	 */
	claimKeyBinding(domainName, pubKeyHash, token, visitReward, verificationReward, forBalance) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		this.contract.claimKeyBinding(hexDomainName, pubKeyHash, token, visitReward, verificationReward, {value: forBalance}).then(() => {
			console.log(`(${domainName}, ${pubKeyHash}) key binding claimed by the default user. Token: ${token}`
				+ ` Definiton of the reward associated with the claim:`
				+ `\n\tVisit reward: ${visitReward}\n\t Verification reward: ${verifReward}\n\tInitial balance: ${forBalance}`);
		});		
	}

	/**
	 * Updates default user's claim. 
	 * @param  domainName         domain in key binding
	 * @param  pubKeyHash         hash of the public key in the binding
	 * @param  token              new token associated with the claim
	 * @param  visitReward        new visit reward
	 * @param  verificationReward new verification reward
	 * @param   forBalance        added to the reward balance associated with
	 *                                  the claim
	 */
	updateClaim(domainName, pubKeyHash, token, visitReward, verificationReward, forBalance) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		this.contract.updateClaim(hexDomainName, pubKeyHash, token, visitReward, verificationReward, {value: forBalance}).then(() => {
			console.log(`Updated default user's claim linked to (${domainName}, ${pubKeyHash}). New token: ${token}`
				+ ` Updates to the reward associated with the claim:`
				+ `\n\tVisit reward: ${visitReward}\n\t Verification reward: ${verifReward}\n\tAdded to balance: ${forBalance}`);
		});
	}

	/**
	 * Default user records a visit to the given key binding.
	 * @param  domainName domain in key binding
	 * @param  pubKeyHash hash of the public key that is part of the binding
	 */
	visitedKeyBinding(domainName,  pubKeyHash) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		this.contract.visitedKeyBinding(hexDomainName, pubKeyHash).then(() => {
			console.log(`Recorded a visit to (${domainName}, ${pubKeyHash})`);
		});
	}

	/**
	 * Default user has recorded a verification of the given claimant's claim.
	 * @param  domainName domain in key binding
	 * @param  pubKeyHash hash of the public key that is part of the binding
	 * @param  claimant   creator of the verified claim
	 */
	verifiedClaim(domainName, pubKeyHash, claimant) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		this.contract.verifiedClaim(hexDomainName, pubKeyHash, claimant).then(() => {
			console.log(`Recorded a verification of claimant's (address: ${claimant}) claim linked to`
				+ ` (${domainName}, ${pubKeyHash})`);
		});
	}

	/**
	 * If default user is the claimant with the most verifications for the 
	 * given key binding, she declares key binding revoked.
	 * @param  domainName domain in key binding
	 * @param  pubKeyHash hash of the public key that is part of the binding
	 */
	declareBindingRevoked(domainName, pubKeyHash) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		this.contract.revokeKeyBinding(hexDomainName, pubKeyHash).then(() => {
			console.log(`Default user is the claimant with the most verifications. (${domainName}, ${pubKeyHash})`
				+ ` key binding is declared revoked.`);
		});
	}

	/**
	 * Sender initialized trust in receiver at the given level and 
	 * sends a certain amount of ether to the funds for posting new 
	 * key bindings in order to show how much she trusts the receiver.
	 * @param  sender             trust initializer
	 * @param  receiver           entity that is trusted
	 * @param  level              trust level 0 - unknown, 1 - untrustworthy, 
	 *                                  2 - marginal, 3 - full
	 * @param  forContractBalance ether sent to the reward fund for posting 
	 *                                  unencountered key bindings
	 */
	trust(sender, receiver, level, forContractBalance) {
		// Check whether trust is already initialized
		let initTrustEvents = this.contract.InitializedTrust({sender: sender, receiver: receiver}, {fromBlock: 0, toBlock: 'latest'});
		let outerThis = this;

		initTrustEvents.get(function(err, res) {
			if (err) {
				console.log('ERROR while getting InitializedTrust events');
			} else {
				console.log(forContractBalance);
				if (res.length > 1) {
					console.log('ERROR Trust initialized more than once');
				} else if (res.length === 1) {
					outerThis.contract.updateTrust(receiver, level, {value: forContractBalance}).then(() => {
						console.log(`${sender} updated trust in ${receiver}: \n\tLevel: ${level}\n\tAdded to previously contributed: ${forContractBalance}`);
					});
				} else {
					outerThis.contract.initTrust(receiver, level, {value: forContractBalance}).then(() => {
						console.log(`${sender} initialized trust in ${receiver}: \n\tLevel: ${level}\n\tInitial contribution: ${forContractBalance}`);
					});
				}
			}
		})
	}

	

	/**
	 * Looks up a certificate of the given key binding stored on IPFS. Certificate
	 * is returned as a hexadecimal string.
	 */
	async lookupCertificate(domainName, pubKeyHash) {
		let hexDomainName = this.web3._extend.utils.fromAscii(domainName);
		let binding = await this.contract.bindings.call(hexDomainName, pubKeyHash);
		
		let certificateHash = binding[5];

		ipfs.cat(certificateHash, function(err, res) {
			if (err) {
				console.log('ERROR occured while trying to access certificate on ipfs, ERROR: ' + err);
			} else {
				console.log("Certificate hex string: ");
				console.log(res);
			}
		})
	}

	/**
	 
	 * @param  {[type]} domain     [description]
	 * @param  {[type]} privKey    [description]
	 * @param  {[type]} pubKeyHash ethAddress    [description]
	 * @return {[type]}            [description]
	 */
	/**
	 * Creates a token object containing domain and token signed with a given ethereum private key. 
	 * This token object can be put on website on 'path/to/token' to allow tki users to make verifications.
	 * @param  domain     domain in key binding
	 * @param  pubKeyHash hash of the public key in key binding
	 * @param  privKey    Ethereum private key used to sign the token object
	 * @param  ethAddress Ethereum address corresponding to the used private key
	 * @return            created token object
	 */
	createToken(domain, pubKeyHash, privKey, ethAddress) {
		let bufPrivKey = ethUtils.toBuffer(privKey);

		if (!ethUtils.isValidPrivate(bufPrivKey)) {
			console.log('ERROR: private key is invalid. Private key should follow the rules of secp256k1 curve');
			return;
		}

		let token = crypto.randomBytes(64).toString('hex');

		let bindingAndTok = {
			domain: domain,
			pubKeyHash: pubKeyHash,
			token: token
		};

		let msgHash = this.web3.sha3(JSON.stringify(bindingAndTok));
		let bufMsgHash = ethUtils.toBuffer(msgHash);

		let ECDSASignature = ethUtils.ecsign(bufMsgHash, bufPrivKey);

		let tokenObj = {
			msg: bindingAndTok,
			signature: ECDSASignature,
			// it is stored with '0x' prefix -- prefix added in tki-bin
			ethAddress: ethAddress
		};

		// so that we can read them later
		tokenObj.signature.r = ethUtils.bufferToHex(tokenObj.signature.r);
		tokenObj.signature.s = ethUtils.bufferToHex(tokenObj.signature.s);

		console.log(JSON.stringify(tokenObj));
		return tokenObj;
	}


	/**
	 * Finds claimant who signed token object and records a verification for
	 * that claim.
	 * @param  domainName domain with a token object
	 */
	verify(domainName) {

		let signed;
		let tokenObj;

		let outerThis = this;

		// Just an example, a json file that contains a domain, token, and signature is expected to be on a specific path on the website,
		// e.g. '/path/to/tokenObject.json'
		request('https://' + domainName + '/path/to/tokenObject.json', function(err, response, body) {
			if (err) {
				console.log('Error while accessing ' + domainName + ' ERROR: ' + err);
			}
			if (response && response.statusCode === 200) {
				tokenObj = JSON.parse(body);

				// Public key should be checked as well
				if (tokenObj.msg.domain !== domainName) {
					console.log('ERROR: visited domain and domain contained in token object do not match');
					return;
				}


				// Recover ethereum address of the signer
				let msgHash = outerThis.web3.sha3(JSON.stringify(tokenObj.msg));
				let bufMsgHash = ethUtils.toBuffer(msgHash);

				let signature = tokenObj.signature;
				signature.r = ethUtils.toBuffer(signature.r);
				signature.s = ethUtils.toBuffer(signature.s);

				// console.log(signature.r);
				let bufPubKey = ethUtils.ecrecover(bufMsgHash, signature.v, signature.r, signature.s);

				let bufAddress = ethUtils.pubToAddress(bufPubKey);
				let address = ethUtils.bufferToHex(bufAddress);

				// console.log('VALID ADDRESS?');
				// console.log(address);
				// console.log(ethUtils.isValidAddress(address));


				// Claimaints maybe do not have to be stored, ono could get it
				// from the sender of transaction containing events
				// Compare the Ethereum address of the signer with the address of every claimaant
				let events = outerThis.contract.ClaimedKeyBinding({domain: domainName}, {fromBlock: 0, toBlock: 'latest'});
				events.get(function(err, res) {
					console.log(res);
					if (err) {
						console.log('ERROR while getting ClaimedKeyBinding events');
						console.log(err);
					} else {
						for (let log of res) {
							let transaction = outerThis.web3.eth.getTransaction(log.transactionHash);

							// Verify claim if the claimant corresponds to token object signer
							if (transaction.from === address) {
								let hexDomain = outerThis.web3._extend.utils.fromAscii(domainName);
								outerThis.contract.verifiedClaim(hexDomain, log.args.kecPubKey, address);
							}
						}
					}
				});
			
			}
		});
	}


	/**
	 * Lists users that trust TKI user with the given Ethereum address.
	 * @param  address   address of TKI user
	 * @param  fromTo    list users trusted by the given address, or users that 
	 *                        trust the given address; trust directed from the 
	 *                        given address is 'true'
	 */
	listTrust(address, fromTo) {

		let initTrustEvents;

		if (fromTo) {
			initTrustEvents = this.contract.InitializedTrust({sender: address}, {fromBlock: 0, toBlock: 'latest'});	
		} else {
			initTrustEvents = this.contract.InitializedTrust({receiver: address}, {fromBlock: 0, toBlock: 'latest'});
		}

		let outerThis = this;

		let trustRelationships = [];
		let commitments = [];
		initTrustEvents.get(function(err, res) {
			if (err) {
				console.log('ERROR while getting InitializedTrust events');
			} else {
				for (let log of res) {
					trustRelationships.push(log.args);
					commitments.push(log.args.amount);
					// console.log(log.args);
					
				}
			}

			let updatedTrustEvents;
			if (fromTo) {
				updatedTrustEvents = outerThis.contract.UpdatedTrust({sender: address}, {fromBlock: 0, toBlock: 'latest'});	
			} else {
				updatedTrustEvents = outerThis.contract.UpdatedTrust({receiver: address}, {fromBlock: 0, toBlock: 'latest'});
			}
			
			updatedTrustEvents.get(function(err, res) {
				if (err) {
					console.log('ERROR while getting UpdatedTrust events');
				} else {
					for (let log of res) {
						let idx;
						if (fromTo) {
							idx = trustRelationships.findIndex(obj => {
								return obj.receiver === log.args.receiver;
							});
						} else {
							idx = trustRelationships.findIndex(obj => {
								return obj.sender === log.args.sender;
							});
						}
						trustRelationships[idx] = log.args;
						commitments[idx] = commitments[idx].plus(log.args.amount);
					}
				}

				if (fromTo) {
					for (let i = 0; i < trustRelationships.length; ++i) {
						console.log(i+1 + ' Account: ' + trustRelationships[i].receiver
							+ '; Level: ' + trustRelationships[i].level.toString()
							+ '; Commitment: ' + commitments[i].toString());
					}
				} else {
					for (let i = 0; i < trustRelationships.length; ++i) {
						console.log(i+1 + ' Account: ' + trustRelationships[i].sender
							+ '; Level: ' + trustRelationships[i].level.toString()
							+ '; Commitment: ' + commitments[i].toString());
					}
				}
			});
		});		
	}


	/**
	 * Lists claims associated with the given key binding.
	 */
	listClaims(domain, pubKeyHash) {
		let hexDomain = this.web3._extend.utils.fromAscii(domain);

		let claimedKeyBindingEvents = this.contract.ClaimedKeyBinding({domain: hexDomain, kecPubKey: pubKeyHash}, {fromBlock: 0, toBlock: 'latest'});

		let outerThis = this;
		let claims = [];

		claimedKeyBindingEvents.get(function(err, res) {
			if (err) {
				console.log('ERROR while getting ClaimedKeyBinding events');
			} else {
				for (let log of res) {
					claims.push(log.args);					
				}
			}

			let updatedClaimEvents = outerThis.contract.UpdatedClaim({domain: hexDomain, kecPubKey: pubKeyHash}, {fromBlock: 0, toBlock: 'latest'})

			updatedClaimEvents.get(function(err, res) {
				if (err) {
					console.log('ERROR while getting UpdatedClaim events');
				} else {
					for (let log of res) {
						let idx = claims.findIndex(obj => {
							return obj.claimant === log.args.claimant;
						});
						claims[idx] = log.args;
					}
				}

				// Print information
				for (let i = 0; i < claims.length; ++i) {
					console.log(i+1 + ' Claimant: ' + claims[i].claimant + '; Token: ' + claims[i].token + '; Compensation on visit: '
						+ claims[i].visitReward + '; Compensation on verification: ' + claims[i].verificationReward);
				}
			})
		});		
	}


	/**
	 * Lists all registered users.
	 */
	listUsers() {
		let newUserEvents = this.contract.NewUser({}, {fromBlock: 0, toBlock: 'latest'});
		let outerThis = this;
		newUserEvents.get(async function(err, res) {
			if (err) {
				console.log('ERROR while getting NewUser events');
			} else {
				for (let i = 0; i < res.length; ++i) {
					// Name might have changed, so look what is stored in contract storage
					let userStruct = await outerThis.contract.users.call(res[i].args.user);
					console.log(i+1 + ' Account: ' + res[i].args.user + '; Name: ' + outerThis.web3._extend.utils.toAscii(userStruct[0]));
				}
			}
		})
	}


	/**
	 * Lists posted key bindings. If argument is undefined, all bindings are listed.
	 * If domain is defined bindings associated with the domain are listed.
	 * @param  domain   associated bindings of this domain are listed
	 */
	listBindings(domain) {
		let newBindingPostedEvents;
		if (domain) {
			newBindingPostedEvents = this.contract.NewBindingPosted({domain: domain}, {fromBlock: 0, toBlock: 'latest'});	
		} else {
			newBindingPostedEvents = this.contract.NewBindingPosted({}, {fromBlock: 0, toBlock: 'latest'});	
		}

		let outerThis = this;
		newBindingPostedEvents.get(async function(err, res) {
			if (err) {
				console.log('ERROR while getting NewBindingPosted events');
			} else {
				for (let i = 0; i < res.length; ++i) {
					let hexDomain = outerThis.web3._extend.utils.toAscii(res[i].args.domain);
					let binding = await outerThis.contract.bindings.call(hexDomain, res[i].args.kecPubKey);
					console.log(i+1 + ' Domain: ' + hexDomain + '; Public key hash: ' + res[i].args.kecPubKey 
						+ '; Poster: ' + binding[0] + '; Strongest Claimant: ' + binding[2] + '; Visits number: ' 
						+ binding[3] + '; Declared revoked: ' + binding[4] + '; Certificate hash: ' + binding[5]
					);
				}
			}
		});
		
	}

	/**
	 * Lists visits recorded for the given domain.
	 */
	listVisits(domain) {
		let userVisitedBindingEvents = this.contract.UserVisitedBinding({domain: domain}, {fromBlock: 0, toBlock: 'latest'});

		userVisitedBindingEvents.get(function(err, res) {
			if (err) {
				console.log('ERROR while getting UserVisitedBinding events');
			} else {
				for (let i = 0; i < res.length; ++i) {
					console.log(i+1 + ' Public key hash: ' + res[i].args.kecPubKey + '; User: ' + res[i].args.user);
				}
			}
		});
	}

	/**
	 * Lists verifications related to the given domain.
	 */
	listVerifications(domain) {
		let verifiedClaimEvents = this.contract.VerifiedClaim({domain: domain}, {fromBlock: 0, toBlock: 'latest'});

		verifiedClaimEvents.get(function(err, res) {
			if (err) {
				console.log('ERROR while getting VerifiedClaim events');
			} else {
				for (let i = 0; i < res.length; ++i) {
					console.log(i+1 + ' Public key hash: ' + res[i].args.kecPubKey + '; Claimant: ' + res[i].args.claimant);
				}
			}
		});
	}

	/**
	 * Given amount is added to the fund used to reward users that post key 
	 * bindings.
	 * @param amount paid in fund
	 */
	addToPostFund(amount) {
		this.contract.addToPostFund({value: amount}).then(() => {
			console.log(`Default user added ${amount} wei to the post fund.`);
		});
	}

	/**
	 * Lists all of the payments to the post fund used to reward posters of 
	 * key bindings.
	 */
	paymentsToPostFund() {
		// {} should be included as a parameter, otherwise only last event is shown
		let addedToPostFundEvents = this.contract.AddedToPostFund({}, {fromBlock: 0, toBlock: 'latest'});

		let fund = new BigNumber(0);

		addedToPostFundEvents.get(function(err, res) {
			if (err) {
				console.log('ERROR while getting AddedToPostFund events');
			} else {
				console.log('Payments to post fund: ');
				for (let i = 0; i < res.length; ++i) {
					console.log(i+1 + ' User: ' + res[i].args.user + '; Amount: ' + res[i].args.amount);
					fund = fund.plus(res[i].args.amount);
				}
			}
			console.log('\nReward fund for posting rewards balance: ' + fund);
		});
	}


	/**
	 * Given data is posted to IPFS and its hash is printed out.
	 * @param  {[type]} data data stored on IPFS
	 */
	postToIPFS(data) {
		ipfs.add(data, function(err, res) {
			if (err) {
				console.log('Error while storing on IPFS: ');
				console.log(err);
			} else {
				console.log(res);
			}
		})	
	}






	// Unfinished -- for visualization
	// async trustChains(account1, account2, level, depthLimit) {
	// 	let chains = [];
	// 	let checkedNodes = [];

	// 	let link = {
	// 		associatedChain: null,
	// 		indexOnAssociatedChain: null,
	// 		chain: [account1]
	// 	};
	// 	console.log(link.chain);
	// 	let finished = await recursive_dls.apply(this, [account1, account2, 0, depthLimit]).then(() => console.log(chains));
	// 	console.log(link.chain);
	// 	console.log(finished);
	// 	// DEBUG -- could contain chains between account or be an empty string
	// 	console.log('CHAINS: ');
	// 	console.log(chains);
	// 	return chains;
	// 	async function recursive_dls(node, goal, depth, limit) {
	// 		let outerThis = this;
	// 		return new Promise(function(resolve, reject) {
	// 			// DEBUG
	// 			if (chains.length > 3) {
	// 				console.log('STOP');
	// 				console.log(chains.length);
	// 				resolve(false);
	// 			}
	// 			console.log('DEPTH: ' + depth);
	// 			console.log('NODE: ' + node);
	// 			// console.log('CHECKEDNODEs');
	// 			// console.log(checkedNodes);
	// 			console.log('LINK CHAIN');
	// 			console.log(link);
	// 			console.log('CURRENT CHAINS: ')
	// 			console.log(chains);
	// 			// console.log('CHAIN LENGTH: ' + link.chain.length);

	// 			console.log(goal);
	// 			console.log(node);

	// 			if (node !== goal) {
	// 				checkedNodes.push(node);
	// 			}
				
	// 			if (node.toLowerCase() === goal.toLowerCase()) {
	// 				chains.push(link);
	// 				console.log('CHAIN LENGTH: ' + link.chain.lenght);
	// 				link = {
	// 					associatedChain: chains.length - 1,
	// 					// index of the node in the AssociatedChain
	// 					nodeOnAssociatedChain: link.chain[link.chain.length - 2],
	// 					chain: []
	// 				};
	// 				resolve(true);
	// 				// return true;
	// 			} else if (depth === limit) {
	// 				console.log('IN CUTOFF');
	// 				// cutoff
	// 				link.chain.pop();
	// 				// return false;
	// 				resolve(true);
	// 			} else {
	// 				console.log('IN ELSE');
	// 				console.log(node);
	// 				// DEBUG -- need or not '0x'
	// 				// console.log(this);
	// 				// console.log('THIS');
	// 				// console.log(outerThis);
	// 				outerThis.getTrustedUsers(node, level, async (trustedUsers) => {
	// 					// console.log('TRUSTEDUSERS IN CALLBACK');
	// 					// console.log(trustedUsers);
	// 					for (let successor of trustedUsers) {
	// 						if (!checkedNodes.includes(successor)) {
	// 							// checkedNodes.push(successor);
	// 							// console.log('NODE IN CALLBACK: ');
	// 							// console.log(node);
	// 							// console.log(trustedUsers);
	// 							link.chain.push(successor);
	// 							await recursive_dls.apply(outerThis, [successor, goal, depth + 1, limit]);
	// 						}
	// 					}
	// 					// remove last node from the current chain
						
						
	// 					if (link.chain.length === 0) {
	// 						// link.indexOnAssociatedChain = link.lengthOnMainChain - 1;
	// 					} else {
	// 						link.chain.pop();
	// 					}
	// 					if (chains[chains.length - 1]) {
	// 						if (chains[chains.length - 1].chain.includes(node)) {
	// 							link.nodeOnAssociatedChain = node;
	// 						}
	// 					}
	// 					resolve(true);	
	// 				});
					
	// 			}
	// 		});
	// 	}
	// }
} 