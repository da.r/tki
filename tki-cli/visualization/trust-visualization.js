// DEBUG -- var vs strict?

var options = {
	groups: {
		mainUsers: {
			shape: 'icon',
			icon: {
				// face: 'FontAwesome',
				code: '\uf007',
				size: 50,
				color: 'orange'
			}
		},
		users: {
			shape: 'icon',
			icon: {
				// face: 'FontAwesome',
				code: '\uf007',
				size: 50,
				color: 'blue'
			}
		}	
	}
}

// var nodes = [{
// 	id: 1,
// 	label: 'User1',
// 	group: 'mainUsers'
// }, {
// 	id: 2,
// 	label: 'User1',
// 	group: 'users'
// }, {
// 	id: 3,
// 	label: 'User1',
// 	group: 'users'
// }, {
// 	id: 4,
// 	label: 'User1',
// 	group: 'users'
// }, {
// 	id: 5,
// 	label: 'User1',
// 	group: 'users'
// }, {
// 	id: 6,
// 	label: 'User1',
// 	group: 'users'
// }, {
// 	id: 7,
// 	label: 'User2',
// 	group: 'mainUsers'
// }];


// var edges = [{
// 	from: 1,
// 	to: 3
// }, {
// 	from: 1,
// 	to: 4
// }, {
// 	from: 2,
// 	to: 4
// }, {
// 	from: 3,
// 	to: 7
// }]

// make requirejs not add .js to the paths
require.config({
    paths: {
        'signalr-hubs': '/signalr/hubs?noext'
    }
});

require(['../index'], function(file) {
	console.log(file);
}); 

load(vizData);

function load(vizData) {
	var container = document.getElementById('visualization');
	var data = {
		nodes: vizData.nodes,
		edges: vizData.edges
	}

	var network = new vis.Network(container, data, options);
};