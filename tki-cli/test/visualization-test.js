'use-strict';


const initLib = require('../index');

describe('test trust chain visualization', function() {
	this.timeout(150000);
	let tki;
	//DEBUG --contract has to be already deployed
	// and uses already existing config
	before(function() {
		console.log('STARTED');
		let config = require('../bin/resources/config.json');
		tki = initLib(config);
		// // registers all of the users in the network
		// // DEBUG - i < num of accounts
		// for (let i = 0; i < 10; ++i) {
		// 	// if not registered
		// 	if (!tki.getUser(tki.web3.eth.accounts[i])[3]) {
		// 		// DEBUG -- should this be async?
		// 		tki.register(tki.web3.eth.accounts[i], 'name' + i, '');
		// 	}
		// }
		// console.log('STARTED');
		// done();
	});

	// DEBUG - have a test with randomized trust, and the one where I know who trusts whom
	beforeEach(function() {
		console.log('STARTED');
		// tki.initUserToUserTrust(tki.web3.eth.accounts[1], tki.web3.eth.accounts[2], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[2], tki.web3.eth.accounts[3], 3, 0);
		// console.log('IN BEFORE EACH');
		// tki.initUserToUserTrust(tki.web3.eth.accounts[3], tki.web3.eth.accounts[4], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[4], tki.web3.eth.accounts[5], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[2], tki.web3.eth.accounts[7], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[7], tki.web3.eth.accounts[1], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[7], tki.web3.eth.accounts[9], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[7], tki.web3.eth.accounts[0], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[3], tki.web3.eth.accounts[6], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[6], tki.web3.eth.accounts[8], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[0], tki.web3.eth.accounts[8], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[8], tki.web3.eth.accounts[4], 3, 0);
		// tki.initUserToUserTrust(tki.web3.eth.accounts[6], tki.web3.eth.accounts[0], 3, 0);
		tki.initUserToUserTrust(tki.web3.eth.accounts[9], tki.web3.eth.accounts[0], 3, 0);
	});

	it('trust chain between 1 and 5', async function() {

		let chains = await tki.trustChains(tki.web3.eth.accounts[1], tki.web3.eth.accounts[0], 0, 10);
		console.log('CHAINS: ');
		console.log(chains);
		console.log('END');
	});

	it('trust chain test from bin', async function() {
		let config = require('../bin/resources/config.json');
				let tki = initLib(config);
				// let account1 = '0x' + argv.account1;
				// let account2 = '0x' + argv.account2;

				let chains = await tki.trustChains(tki.web3.eth.accounts[1], tki.web3.eth.accounts[0], 0, 10);

				console.log('WORKING');
				console.log(chains);
				let nodes = [];
				let edges = [];
				let id = 1;
				let edgeObj = {};

				let accountToId = {};
				for (let i = 0; i < chains.length; ++i) {
					for (let j = 0; j < chains[i].chain.length; ++j) {
						let name = tki.web3._extend.utils.toAscii(tki.users.call(chains[i].chain[j])[1]);

						let nodeObj = {
							id: id,
							label: name,
							group: 'users'
						};

						accountToId[chains[i].chain[j]] = id;

						id = id + 1;

						if ((chains[i].chain[j] === account1) || (chains[i].chain[j] === account2)) {
							nodeObj.group = 'mainUsers';
						}

						nodes.push(nodeObj);

						// if this is the first chain (the one from account1)
						if (i === 0) {
							if (j === 0) {
								edgeObj.from = id;
							}
							// DEBUG -- maybe not needed, bcs after else control path edgeObj isn't pushed  
							else if (j === (chains[i].chain.length - 1)) {
								edgeObj.to = id;
								edges.push(edgeObj);
							}
							else {
								edgeObj.to = id;
								edges.push(edgeObj);
								edgeObj.from = id;
							}
						} else {
							if (j === 0) {
								// let chainIndex = chains[i].associatedChain;
								// DEBUG -- consistent naming! - should be accountOnAsso...
								let nodeId = chains[i].nodeOnAssociatedChain;
								edgeObj.from = nodeId;
								edgeObj.to = chains[i].chain[j];
								edges.push(edgeObj);
								edgeObj.from = chains[i].chain[j];
							} else {
								edgeObj.to = id;
								edges.push(edgeObj);
								edgeObj.from = id;
							}
						}

					}
				}

				let vizData = {
					nodes: nodes,
					edges: edges
				};

				// sync because opn should run after it is finished
				fs.writeFileSync('visualization/trust-visualization.json', JSON.stringify(vizData), 'utf8', function(err) {
					if (err) {
						return console.log(err);
					}
				});

				opn('./visualization/trust-visualization.html');
	})


});