#!/usr/bin/env node

'use-strict';

const initLib = require('../index');

const fs = require('fs');
const util = require('util');
const yargs = require('yargs');

// used for opening browser
const opn = require('opn');

const argv = yargs
	.version()
	.usage('Usage: tki <command> [option] [arg]')
		

	// Definition of command 'config'
	.command(['config', 'conf'], 'get/modify configuration', function(yargs) {
		return yargs
			.option('set', {
				alias: 's',
				description: 'set configuration content',
				boolean: true
			})
			.option('host', {
				alias: 'h',
				description: 'argument to set host'
			})
			.option('port', {
				alias: 'p',
				description: 'argument to set port'
			})
			.option('contract-address', {
				alias: 'c',
				description: 'argument to set contract address'
			})
			.option('default-account', {
				alias: 'a',
				description: 'argument to set default account'
			})
			.option('default-gas', {
				alias: 'g',
				description: 'argument to set default gas limit'
			})
	}, function(argv) {
		let config;
		if (fs.existsSync('bin/resources/config.json')) {
			config = require('./resources/config.json');	
		} else {
			config = {};
		}
		
		if (argv.set) {
			if (argv.host) {
				config.host = argv.host;
			}
			if (argv.port) {
				config.port = argv.port;
			}
			if (argv['contract-address']) {
				config.contractAddress = argv['contract-address'];
			}
			if (argv['default-account']) {
				config.defaultAccount = argv['default-account'];
			}
			if (argv['default-gas']) {
				config.defaultGas = argv['default-gas'];
			}

			fs.writeFile('bin/resources/config.json', JSON.stringify(config), 'utf8', function(err) {
				if (err) {
					return console.log(err);
				}
			})
		} else {
			console.log('\nConfiguration content:\n');
			console.log(config);
		}
	})
	.example('tki config', 'gets configuration')
	.example('tki config -s -c 7161A548512AF794D8C069667BcDD327Fa918358\n', 'sets contract addresss to the given address')
	.example('tki config -s -h "127.0.0.1" -p 7545 -c 7161A548512AF794D8C069667BcDD327Fa918358 -a' 
		+ ' fb447c9e9af93384608b2c2eba6c37228225e8a4 -g 300000\n', 'sets host, port, contract address, default account,' 
		+ ' and default gas to the specified values')


	// Definition of command 'list'
	.command(['list', 'l'], 'lists users or key bindings', function(yargs) {
		return yargs
			.option('users', {
				alias: 'u',
				description: 'lists all registered TKI users',
				boolean: true
			})
			.option('trust-to', {
				alias: 't',
				description: 'list users that have established a trust relationship with the given user; if used as' 
				+ ' a boolean option, default account is considered to be the given user'
			})
			.option('trust-from', {
				alias: 'f',
				description: 'list users towards which the given user has esablished trust relationship; if used as'
				+ ' a boolean option, default account is considered to be the given user'
			})
			.option('bindings', {
				alias: 'b',
				description: 'lists posted key bindings'
			})
			.options('visits', {
				alias: 'v',
				description: 'lists all recorded visits related to the given domain'
			})
			.options('verifications', {
				alias: 'w',
				description: 'lists all recorded verifications related to the given domain'
			})
	}, function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		if (argv.users) {
			if (argv.trustTo) {
				if (argv.trustTo === true) {
					console.log('Users that trust ' + config.defaultAccount + ': ');
					tki.listTrust('0x' + config.defaultAccount, false);
				} else {
					console.log('Users that trust ' + argv.trustTo + ': ');
					tki.listTrust('0x' + argv.trustTo, false);
				}
				
			} else if (argv.trustFrom) {
				if (argv.trustFrom === true) {
					console.log('Users trusted by ' + config.defaultAccount + ': ');
					tki.listTrust('0x' + config.defaultAccount, true);
				} else {
					console.log('Users trusted by ' + argv.trustFrom + ': ');
					tki.listTrust('0x' + argv.trustFrom, true);
				}
			} else {
				console.log('All registered users: ');
				tki.listUsers();
			}

		} else if (argv.bindings) {
			if (argv.bindings === true) {
				console.log('All posted key bindings: ');
				tki.listBindings();
			} else {
				console.log('Bindings associated with domain \'' + argv.bindings + '\': ');
				tki.listBindings(tki.web3.fromAscii(argv.bindings));
			}
		} else if (argv.visits) {
			console.log('Visits associated with domain \'' + argv.visits +'\': ');
			tki.listVisits(tki.web3.fromAscii(argv.visits));
		} else if (argv.verifications) {
			console.log('Verifications associated with domain \'' + argv.verifications +'\': ');
			tki.listVerifications(tki.web3.fromAscii(argv.verifications));
		}
			
	})
	.example('tki list -u', 'lists all of the registered TKI users')
	.example('tki list -u -t 08e723858c289f5d68ef4c5207060995fb89969c', 'lists all of the users trusted by the given account')
	.example('tki list -u -f 08e723858c289f5d68ef4c5207060995fb89969c', 'lists all of the users that trust the given account')
	.example('tki list -b', 'lists all of the key bindings posted to TKI')
	.example('tki list -b example.com', 'lists all of the key bindings for the given domain')
	.example('tki list -v example.com', 'lists all of the visits for the given domain')
	.example('tki list -w example.com\n', 'lists all of the verifications for the given domain')
	

	// Definition of command 'register'
	.command(['register <name> [info]', 'reg'], 'registers a new user', () => {}, async function(argv) {

		let config = require('./resources/config.json');
		let tki = initLib(config);

		if (argv.info) {
			// 0x added, because if arguments use 0x notation they are automatically converted to decimal numbers
			tki.register('0x' + config.defaultAccount, argv.name, argv.info);
		} else {
			tki.register('0x' + config.defaultAccount, argv.name, '');
		}
	})
	.example('tki register John', 'register default Ethereum account with name "John" and no user information')
	.example('tki register John "Info1-location: Stuttgart, Germany;Info2-email: john@org.com"\n', 
		'register default account in config with name "John" and two information pieces separated by \';\'')


	// Definition of command 'update-info'
	.command(['update-info <name> [info]', 'ui'], 'updates user info', () => {}, function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		if (argv.info) {
			// 0x added, because if arguments use 0x notation they are automatically converted to decimal numbers
			tki.updateUserInfo('0x' + config.defaultAccount, argv.name, argv.info);
		} else {
			tki.updateUserInfo('0x' + config.defaultAccount, argv.name, '');
		}
	})
	.example('tki update-info Johnny "Info1-location: Stuttgart, Germany; Info2-email: john@org.com"\n')
	.demandCommand(1, 'New / old name is obligatory.')


	// Definition of command 'post-fund'
	.command(['post-fund', 'pf'], 'get payments and balance or add to reward fund for posting rewards; payments option'
		+ ' has precedence', function(yargs) {
		return yargs
			.option('add', {
				alias: 'a',
				description: 'add to post fund',
				default: 0
			})
			.options('payments', {
				alias: 'p',
				description: 'get all payments to the fund',
				boolean: true
			})
	}, 
	function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		if (argv.payments) {
			let fund = tki.paymentsToPostFund();
			
		}  else if (argv.add) {
			tki.addToPostFund(argv.add);
		}
	})
	.example('tki post-fund -p', 'gets all of the payments and the current balance of the post fund')
	.example('tki post-fund -a 500\n', 'adds 500 wei to post fund')


	// Definition of command 'post-ipfs'
	.command(['post-ipfs <data>', 'pi'], 'posts data to ipfs and prints hash / link to it', () => {},
	function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		tki.postToIPFS(argv.data);
	})
	.example('tki post-ipfs 3082074030820628a[,,,]510a18a0a8088cd\n', 'stores given data to ipfs')


	// Definition of command 'post-binding'
	.command(['post-binding <domain> <pubKeyHash>', 'pb'], 'post a key binding', function(yargs) {
		return yargs
			.option('visit-reward', {
				alias: 'v',
				description: 'reward given when a visit is recordeed',
				default: 0
			})
			.option('verif-reward', {
				alias: 'f',
				description: 'reward given when a verification is recorded',
				default: 0
			})
			.option('for-balance', {
				alias: 'b',
				description: 'ether in weis used for payouts to each visitor or verifier; has to be larger than'
				+ ' 1,000,000 to be accepted',
				requiresArg: true,
				demandOption: 'specify ether balance for issuing visitor rewards'
			})
			.option('certificate-hash', {
				alias: 'c',
				description: 'hash of the certificate posted to IPFS'
			})
	},
	function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);
		if (argv.certificateHash) {
			tki.postKeyBinding(argv.domain, '0x' + argv.pubKeyHash, argv.visitReward, argv.verifReward, 
				argv.certificateHash, argv.forBalance);	
		} else {
			tki.postKeyBinding(argv.domain, '0x' + argv.pubKeyHash, argv.visitReward, argv.verifReward, '', 
				argv.forBalance);
		}
		
	})
	.example('tki post-binding example.com b398eb4f7bf9cc2f5cd6d80051355632abe4d15ee5ed1157971c743ed4295a03' + 
		' -v 10000 -f 200000 -b 1000000 -c QmbfkYup8jTV4Dp52bW9jzY6V9Z3M2D5wrjxxbGsj2Tm1g\n', 'posts a key' +
		' binding example.com with the specified public key hash, payout on visit of 10000, payout on' + 
		' verification 20000, initial balance of 10000000 and the specified certificate hash')
	.demandCommand(2, 'Domain name, public key and amount for balance are obligatory')


	// Definition of command 'lookup-cert'
	.command(['lookup-cert <domain> <pub-key-hash>', 'lc'], 'looks up a certificate stored on IPFS for the given binding', 
		() => {}, function(argv) {
		
		let config = require('./resources/config.json');
		let tki = initLib(config);
		tki.lookupCertificate(argv.domain, '0x' + argv.pubKeyHash);
	})
	.example('tki lookup-cert example.com b398eb4f7bf9cc2f5cd6d80051355632abe4d15ee5ed1157971c743ed4295a03', 
		'looks up a certificate stored on IPFS associated with the specified domain and public key hash')



	// Definition of command 'reward'
	.command('reward [boolOption] [optionArguments]', 'allows user to list existing rewards, or create or' + 
		' update the reward; if both boolean options are set --list takes precedence over --create and --update', 
	function(yargs) {
		return yargs
			.option('create', {
				alias: 'c',
				description: 'creates a reward with specified balance and visit and verification payments',
				boolean: true
			})
			.option('update', {
				alias: 'u',
				description: 'updates the reward; given balance is added to the existing balance, while visit and'
				+ ' verification conpensations are newely defined as given',
				boolean: true
			})
			.option('list', {
				alias: 'l',
				description: 'lists all of the rewards associated with the specified key binding; domain and public'
				+ ' key hash should be specified',
				boolean: true
			})
			.option('domain', {
				alias: 'd',
				description: 'domain name in the key binding whose reward should be modified',
				requiresArg: true,
				demandOption: 'specify domain name of the reward'
			})
			.option('pub-key-hash', {
				alias: 'p',
				description: 'public key hash in the key binding whose reward should be modified',
				requiresArg: true,
				demandOption: 'specify public key hash of the reward'
			})
			.option('visit-reward', {
				alias: 'v',
				description: 'ether that is given to each visitor',
				default: 0,
				demandOption: 'specify reward that the visitors gets'
			})
			.option('verif-reward', {
				alias: 'f',
				description: 'ether that is given each time a verification is recorded',
				default: 0,
				demandOption: 'specify reward that the verifier gets'
			})
			.option('for-balance', {
				alias: 'b',
				description: 'ether used to pay visit reward to each visitor',
				default: 0,
				demandOption: 'specify ether balance for issuing visitor rewards'
			})
	}, function(argv) {
			let config = require('./resources/config.json');
			let tki = initLib(config);

			if (argv.list) {
				tki.listBindingRewards(argv.domain, '0x' + argv.pubKeyHash);
			} else if (argv.create) {
				tki.createBindingReward(argv.domain, '0x' + argv.pubKeyHash, argv.visitReward, argv.verifReward, argv.forBalance);
			} else if (argv.update) {
				tki.updateBindingReward(argv.domain, '0x' + argv.pubKeyHash, argv.visitReward, argv.verifReward, argv.forBalance);
			}
	})
	.example('tki reward -l -d example.com -p b398eb4f7bf9cc2f5cd6d80051355632abe4d15ee5ed1157971c743ed4295a03', 
		'lists all of the rewards associated with the key binding')
	.example('tki reward -c -d example.com -p b398eb4f7bf9cc2f5cd6d80051355632abe4d15ee5ed1157971c743ed4295a03'
		+ ' -v 100 -f 100 -b 1000', 'creates new reward with the specified payouts and balance')
	.example('tki reward -u -d example.com -p b398eb4f7bf9cc2f5cd6d80051355632abe4d15ee5ed1157971c743ed4295a03'
		+ ' -v 100 -f 100 -b 1000\n', 'updates the reward on the given key binding with the specified payouts and balance')


	// Definition of command 'claim'
	.command('claim <domain> <pub-key-hash>', 'list claims associated with a key binding or claim key binding; reward' +
		' associated with the claim can be additionally created; --list option takes precedence if multiple options'
		+ ' are specified; --list is the default option as well', function(yargs) {
		return yargs
			.option('list', {
				alias: 'l',
				description: 'list claims',
				boolean: true
			})
			.option('create', {
				alias: 'c',
				description: 'create a claim',
				boolean: true
			})
			.option('update', {
				alias: 'u',
				description: 'update existing claim',
				boolean: true
			})
			.option('token', {
				alias: 't',
				description: 'token associated with the claim',
				default: ''
			})
			.option('visit-reward', {
				alias: 'v',
				description: 'reward that an informative/voluntary vistor of the website gets',
				default: 0
			})
			.option('verif-reward', {
				alias: 'f',
				description: 'reward that a user who verifies token and the signature gets',
				default: 0
			})
			.option('for-balance', {
				alias: 'b',
				description: 'ether to send to contract',
				default: 0
			})
	}, function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		if (argv.list) {
			tki.listClaims(argv.domain, '0x' + argv.pubKeyHash);
		} else if (argv.create) {
			tki.claimKeyBinding(argv.domain, '0x' + argv.pubKeyHash, argv.token, argv.visitReward, argv.verifReward, 
								argv.forBalance);
		} else if (argv.update) {
			tki.updateClaim(argv.domain, '0x' + argv.pubKeyHash, argv.token, argv.visitReward, argv.verifReward, 
							argv.forBalance);
		} else {
			tki.listClaims(argv.domain, '0x' + argv.pubKeyHash);
		}
	})
	.example('tki claim -l <domain> <pubKeyHash>', 'lists claims associated with the specified key binding')
	.example('tki claim -c <domain> <pubKeyHash> -t <token>', 
		'creates a claim for the given key binding with the given token')
	.example('tki claim -u <domain> <pubKeyHash> -t <token>', 
		'updates the associated with the given key binding with the new token')
	.example('tki claim -u <domain> <pubKeyHash> -t <token> -v 100 -f 200 -b 1000\n', 
		'updates the claim and the associated reward')
	.demandCommand(2, 'Domain namea and public key hash are obligatory.')


	// Definition of command 'declare-revoked'
	.command(['declare-revoked <domain-name> <pub-key-hash>', 'dr'], 'declare specified key binding revoked;' 
		+ ' can be called only if the caller is the strongest claimant', () => {}, function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		tki.declareBindingRevoked(argv.domainName, '0x' + argv.pubKeyHash);
	})
	.example('tki declare-revoked example.com b398eb4f7bf9cc2f5cd6d80051355632abe4d15ee5ed1157971c743ed4295a03\n', 
		'if default account is the strongest claimant, specified key binding (domain and public key hash shown) is'
		+ ' declared revoked')
	

	// Definition of command 'visited-binding' -- only for prototype
	.command(['visited-binding <domain-name> <pub-key-hash>', 'vb'], 'record a visit for specified binding', () => {}, 
	function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		tki.visitedKeyBinding(argv.domainName, '0x' + argv.pubKeyHash);
	})
	.example('tki visited-binding example.com b398eb4f7bf9cc2f5cd6d80051355632abe4d15ee5ed1157971c743ed4295a03',
		'records a visit to the specified key binding')


	// Definition of command 'verified-claim' -- only for prototype
	.command(['verified-claim <domain-name> <pub-key-hash> <claimant>', 'vc'], 
		'record a verification for specified binding and claimant', () => {}, function(argv) {

		let config = require('./resources/config.json');
		let tki = initLib(config);

		tki.verifiedClaim(argv.domainName, '0x' + argv.pubKeyHash, '0x' + argv.claimant);
	})
	.example('tki verified-claim example.com <pub-key-hash> <claimant\'s address>',
		'records a verification to the specified key binding and the claimant')


	// Definition of command 'trust'
	.command(['trust <TKIuserAddress> <level>', 'trust given user at the specified level; existing trust relationships' 
		+ ' is updated, i.e. level is changed and ether is added to balance'], '', function(yargs) {
		return yargs
			.option('commitment', {
				alias: 'c',
				description: 'ether for contract balance to indicate the amount of trust',
				default: 0
			})
	}, function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		tki.trust('0x' + config.defaultAccount, '0x' + argv.TKIuserAddress, argv.level, argv.commitment);
	})
	.example('tki trust fb447c9e9af93384608b2c2eba6c37228225e8a4 3', 
		'default user trust the specified TKI user at level 3, i.e. \'full\' trust')
	

	// Definition of command 'create-token'
	.command(['create-token <domain> <pubKeyHash> <priv-key>', 'ct'], 'creates a token object (in JSON) that can be put on a website', 
		() => {}, function(argv) {

		let config = require('./resources/config.json');
		let tki = initLib(config);

		tki.createToken(argv.domain, '0x' + argv.pubKeyHash, '0x' + argv.privKey);
	})


	// Definition of command 'verify'
	.command('verify <domain>', 'verifies the claim on the domain', () => {}, function(argv) {
		let config = require('./resources/config.json');
		let tki = initLib(config);

		tki.verify(argv.domain);
	})



	.help()
	.wrap(yargs.terminalWidth())
	.argv;





	// Visualization -- unfinished
	// .command(['visualization <account1> <account2> [options]', 'vis'], 
	// 		'visualization of trust relationship between two accounts', function(yargs) {
	// 			return yargs
	// 				.option('level', {
	// 					alias: 'l',
	// 					description: 'lowest level of the trust relationship between two node on the chain between'
	// 						+ ' given accounts',
	// 					default: 2
	// 				})
	// 				.option('depth-limit', {
	// 					alias: 'd',
	// 					description: 'depth to search, highest amount of links on the trust chain between accounts',
	// 					default: 5
	// 				})
	// 		}, async function(argv) {
	// 			console.log(argv);
	// 			let config = require('./resources/config.json');
	// 			let tki = initLib(config);
	// 			let account1 = '0x' + argv.account1;
	// 			let account2 = '0x' + argv.account2;

	// 			let chains = await tki.trustChains(account1, account2, argv.l, argv.d);

	// 			console.log('WORKING');
	// 			console.log(chains);
	// 			console.log(chains.length);
	// 			let nodes = [];
	// 			let edges = [];
	// 			let id = 1;
	// 			let edgeObj = {};

	// 			let accountToId = {};
	// 			for (let i = 0; i < chains.length; ++i) {
	// 				for (let j = 0; j < chains[i].chain.length; ++j) {
	// 					console.log('WORKING');
	// 					let name = tki.web3._extend.utils.toAscii((await tki.getUser(chains[i].chain[j]))[1]);
	// 					console.log('WORKING');

	// 					let nodeObj = {
	// 						id: id,
	// 						label: name,
	// 						group: 'users'
	// 					};

	// 					accountToId[chains[i].chain[j]] = id;

	// 					id = id + 1;

	// 					if ((chains[i].chain[j] === account1) || (chains[i].chain[j] === account2)) {
	// 						nodeObj.group = 'mainUsers';
	// 					}

	// 					nodes.push(nodeObj);

	// 					// if this is the first chain (the one from account1)
	// 					if (i === 0) {
	// 						if (j === 0) {
	// 							edgeObj.from = id;
	// 						}
	// 						// DEBUG -- maybe not needed, bcs after else control path edgeObj isn't pushed  
	// 						else if (j === (chains[i].chain.length - 1)) {
	// 							edgeObj.to = id;
	// 							edges.push(edgeObj);
	// 						}
	// 						else {
	// 							edgeObj.to = id;
	// 							edges.push(edgeObj);
	// 							edgeObj.from = id;
	// 						}
	// 					} else {
	// 						if (j === 0) {
	// 							// let chainIndex = chains[i].associatedChain;
	// 							// DEBUG -- consistent naming! - should be accountOnAsso...
	// 							let nodeId = chains[i].nodeOnAssociatedChain;
	// 							edgeObj.from = nodeId;
	// 							edgeObj.to = chains[i].chain[j];
	// 							edges.push(edgeObj);
	// 							edgeObj.from = chains[i].chain[j];
	// 						} else {
	// 							edgeObj.to = id;
	// 							edges.push(edgeObj);
	// 							edgeObj.from = id;
	// 						}
	// 					}

	// 				}
	// 			}

	// 			let vizData = {
	// 				nodes: nodes,
	// 				edges: edges
	// 			};

	// 			// write a .js file
				

	// 			// sync because opn should run after it is finished
	// 			// fs.writeFileSync('visualization/viz-data.json', JSON.stringify(vizData), 'utf8', function(err) {
	// 			// 	if (err) {
	// 			// 		return console.log(err);
	// 			// 	}
	// 			// });

	// 			opn('./visualization/trust-visualization.html');
	// 		})