'use strict';

var web3Provider;
var web3;
var contracts = {};
var tkiInstance;
var accounts;

var sessionDomainsSeen = {};
var bindingsToPostInSession = {};

// Transition types on which we should forget seen domains
// Only some of them are supported by Firefox
var resetSeenDomainsOn = ['link', 'typed', 'auto_bookmark', 'start_page', 'reload', 'keyword'];
var potentiallyToRemove = {};
var trustedUsers = new Set();

var ipfs = new IPFS({host: 'ipfs.infura.io', protocol: 'https'});


async function connectToNode() {
	console.log('Connecting to Blockchain node...');

	web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');

	web3 = new Web3(web3Provider);
	
	$.getJSON('Tki.json', async function(data) {
		// Contract artifact file instantiated with truffle-contract
		let TkiArtifact = data;
		contracts.Tki = TruffleContract(TkiArtifact);

		// Provider for the contract
		contracts.Tki.setProvider(web3Provider);
		await setUpAccountAndInstance();
	})
}

async function setUpAccountAndInstance() {
	tkiInstance = await contracts.Tki.deployed();
	// Set one of the ganache accounts as the default one 
	web3.eth.getAccounts(function(err, res) {
		accounts = res;
		web3.eth.defaultAccount = accounts[0];
	});
}

browser.runtime.onInstalled.addListener(function(details) {
	console.log("TKI Extension installed.\n");

	connectToNode();

	// Storage on the client accesible from browser extension
	// Settings:
	// sendsVotes -- true if the user want to post bindings and record votes
	// trustLevel -- established trust relationships with the lower trust level
	// 		are not considered reliable in trust chains
	// trustedChainLength -- maximum length of the trust chain to a certain user
	// afterBlock -- only recorded visits and verifications after this block are 
	// 		considered
	browser.storage.local.set({
		sendsVotes: false,
		trustLevel: 2,
		trustedChainLength: 3,
		afterBlock: 0,
	});

	console.log("TKI Extension started.\n")
});
	
browser.runtime.onStartup.addListener(function() {
	connectToNode();

	console.log('TKI Exstension started.\n');
});

browser.webRequest.onHeadersReceived.addListener(async function(details) {

	console.log(`Intercepted a request for ${details.url} with ID ${details.requestId}`);

	let securityInfo = await browser.webRequest.getSecurityInfo(details.requestId, {
		certificateChain: false,
		rawDER: true
	});

	// Drop it if no certificate 
	if (securityInfo.state === 'insecure') {
		console.log('No certificate, insecure connection.');
		return;
	} 

	let currentDomain = details.url.split('//')[1].split('/')[0];
	if (Object.keys(sessionDomainsSeen).includes(currentDomain)) {
		console.log('Domain already seen, SKIPPING');
		return;
	}

	// If this is uncommented, domains whose certificates cannot be
	// processed are logged as well
	// sessionDomainsSeen[currentDomain] = {};

	if (currentDomain === 'ipfs.infura.io:5001') {
		console.log('storing cert to ipfs, ignore this connection');
		return;
	}

	console.log('Processing ceritificate...');
	let certificate = securityInfo.certificates[0];
	let rawDER = certificate.rawDER;

	let hexCert = toHexString(rawDER);

	let bytesCert = forge.util.hexToBytes(hexCert);
	let asn1 = forge.asn1.fromDer(bytesCert);
	let cert = forge.pki.certificateFromAsn1(asn1);

	let pemCert = forge.pki.publicKeyToPem(cert.publicKey);
	
	// Check whether user is registered and wants to send certs and vote
	let user = await tkiInstance.users.call(web3.eth.defaultAccount);
	// Second element of the received array is registered attribute
	let registered = user[1];

	let inStorage = await browser.storage.local.get('sendsVotes');
	
	let pubKeyDigest = web3.sha3(pemCert);


	// stop if this binding was already considered
	if (sessionDomainsSeen[currentDomain] !== undefined) {
		if (sessionDomainsSeen[currentDomain].keyHash === pubKeyDigest) {
			return;
		}
	}

	sessionDomainsSeen[currentDomain] = { 
		domainName: currentDomain,
		keyHash: pubKeyDigest,
		certificate: hexCert
	};

	let bindingStruct = await tkiInstance.bindings.call(web3.fromAscii(currentDomain), pubKeyDigest);

	// User registered and wants to send votes?
	if (registered && inStorage.sendsVotes) {

		let numVisits = bindingStruct[3];
		// Post binding or record the visit?
		if (numVisits > 0) {
			// If default user already visited the binding, transaction is reverted
			tkiInstance.visitedKeyBinding(web3.fromAscii(currentDomain), pubKeyDigest, {gas: 5000000});
		} else {
			bindingsToPostInSession[currentDomain] = pubKeyDigest;
		}
	}


	// start preparing info for the popup

	sessionDomainsSeen[currentDomain].strongestClaimant = bindingStruct[2];
	sessionDomainsSeen[currentDomain].verifStrongestClaimant = bindingStruct[1];

	let afterBlock = (await browser.storage.local.get('afterBlock')).afterBlock;
	let allDomVisitEvents = await tkiInstance.UserVisitedBinding({domain: web3.fromAscii(currentDomain)}, {fromBlock: afterBlock, toBlock: 'latest'});
	allDomVisitEvents.get(async function(err, res) {

	 	let trustedDomVisits = 0;
	 	let allBindingVisits = 0;
	 	let trustedBindingVisits = 0;
	 	for (let event of res) {
	 		if (trustedUsers.has(event.args.user)) {
	 			trustedDomVisits += 1;
	 		}
	 		if (event.args.kecPubKey === sessionDomainsSeen[currentDomain].keyHash) {
	 			allBindingVisits += 1;
	 			if (trustedUsers.has(event.args.user)) {
	 				trustedBindingVisits += 1;
	 			}
	 		}
	 	}

	 	sessionDomainsSeen[currentDomain].allDomVisits = res.length;
	 	sessionDomainsSeen[currentDomain].trustedDomVisits = trustedDomVisits;
	 	sessionDomainsSeen[currentDomain].allBindingVisits = allBindingVisits;
	 	sessionDomainsSeen[currentDomain].trustedBindingVisits = trustedBindingVisits;
	});

	let allBindingVerifEvents = await tkiInstance.VerifiedClaim({
		domain: web3.fromAscii(currentDomain), 
		kecPubKey: sessionDomainsSeen[currentDomain].keyHash
	}, { 
		fromBlock: afterBlock, 
		toBlock: 'latest'
	});

	allBindingVerifEvents.get(async function(err, res) {

	 	let trustedVerifications = 0;
	 	let trustedVerifStrongestClaimant = 0;
	 	for (let event of res) {
	 		// get the sender of verification
	 		web3.eth.getTransaction(event.transactionHash, function(err, res) {
	 			if (trustedUsers.includes(res.from)) {
	 				trustedVerifications += 1;
	 				if (strongestClaimant === event.args.claimant) {
	 					trustedVerifStrongestClaimant += 1;
	 				}
	 		}
	 		});
	 	}

	 	sessionDomainsSeen[currentDomain].allVerifications = res.length;
	 	sessionDomainsSeen[currentDomain].trustedVerifications = trustedVerifications;
	 	sessionDomainsSeen[currentDomain].trustedVerifStrongestClaimant = trustedVerifStrongestClaimant;
	});

	// Collect info from rewards stored in the contract
	let rewards = await tkiInstance.getRewards(web3.fromAscii(currentDomain), sessionDomainsSeen[currentDomain].keyHash);

	let sumOfVisitRewards = new BigNumber(0);
	let sumOfVerifRewards = new BigNumber(0);
	let sumOfRewardBalances = new BigNumber(0);

	for (let i = 0; i < rewards[0].length; ++i) {
		// is the visit reward inactive?
		if (!rewards[4][i]) {
			sumOfVisitRewards = sumOfVisitRewards.plus(rewards[2][i]);
		}
		// is the verification reward inactive?
		if (!rewards[5][i]) {
			sumOfVerifRewards = sumOfVerifRewards.plus(rewards[3][i]);
		}

		sumOfRewardBalances = sumOfRewardBalances.plus(rewards[1][i]);
	}

	sessionDomainsSeen[currentDomain].sumOfVisitRewards = sumOfVisitRewards;
	sessionDomainsSeen[currentDomain].sumOfVerifRewards = sumOfVerifRewards;
	sessionDomainsSeen[currentDomain].sumOfRewardBalances = sumOfRewardBalances;



	



}, {urls: ['<all_urls>']}, ['blocking']);


// taken from https://bitcoin.stackexchange.com/questions/52727/byte-array-to-hexadecimal-and-back-again-in-javascript
function toHexString(byteArray) {
  return Array.prototype.map.call(byteArray, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('');
}



browser.webNavigation.onBeforeNavigate.addListener(function(details) {
	potentiallyToRemove = sessionDomainsSeen;
});


browser.webNavigation.onCommitted.addListener(function(details) {
	
	// Remove from seen domains those that belong to the previous session / visit
	// if user prompted transition
	if (resetSeenDomainsOn.includes(details.transitionType)) {
		for (let domain of Object.keys(potentiallyToRemove)) {
			if (!(domain in Object.keys(sessionDomainsSeen))) {
				delete sessionDomainsSeen[domain];
				delete bindingsToPostInSession[domain]
			}
		}
	}
});


/**
 * Uses information given in the prompt to post a key binding.
 * @param  rewardAndWei  contains comma-separated wei for initial balance and
 * compensations for recorded visit or verification
 * @param  currentDomain domain in key binding
 * @param  pubKeyDigest  public key hash
 */
async function onExecutedPrompt(rewardAndWei, currentDomain, pubKeyDigest) {

	// Timing storing on ipfs vs storing the whole certificate
	console.log("Timing IFPS started");
	let start = performance.now();

	// what if user clicks cancel when prompt is shown?
	if (rewardAndWei[0] === null) {
		console.log('Canceled posting key binding');
		return;
	}
	let split = rewardAndWei[0].split(',');
	let visitReward = split[0];
	let verificationReward = split[1];
	let wei = split[2];

	ipfs.add(sessionDomainsSeen[currentDomain].certificate, async function(err, res) {
		if (err) {
			console.log('Error while storing on IPFS: ');
			console.log(err);
		} else {
			// console.log('IPFS - certificate hash');
			// console.log(res);
			await tkiInstance.postKeyBinding(web3.fromAscii(currentDomain), pubKeyDigest, visitReward, verificationReward, res, {value: wei, gas: 5000000});
			delete bindingsToPostInSession[currentDomain]
		}

		let end = performance.now();
		console.log("Timing IPFS ended")
		console.log('Storing certificate on IPFS took ' + (end - start) + ' ms');
	})	
}


/**
 * 	Executed when user clicks on 'Post Bindings' button. Injects prompts to
 * 	website that show user key bindingings and ask her to define the reward
 *  for the key binding.
 */
async function postBindings() {
	for (let domain in bindingsToPostInSession) {
		console.log('Posting binding: ' + domain + ' ' + bindingsToPostInSession[domain]);
		let promptVisitReward = `prompt('Post the unencountered key binding: \\n` + 
		`(${domain}, ${bindingsToPostInSession[domain]})\\nEnter initial visit and verification rewards as well` +
		` as the amount of wei to transfer for reward balance. Transaction will be reverted if the amount for balance` +
		` is lower than 1,000,000 wei! Values should be separated by comma. \\n(click  \\'cancel\\' to prevent` +
		` posting of key binding)\\n\\nExample with 1 wei` +
		` visit reward, 2 wei verification reward and 5 wei for balance to transfer: \\'1,2,5\\'\\n', 0)`;
		let result = await browser.tabs.executeScript({code: promptVisitReward});
		onExecutedPrompt(result, domain, bindingsToPostInSession[domain]);
	}
}


/**
 * Updates the set of trusted users used in presentation of the number of trusted
 * visits and verifications. For example, if the accepted length of the trust
 * chain is changed, this will reflect in the recreated set of trusted users.
 */
async function updateTrustedUsers() {
	// Update the set of trusted users
	// Create a set of all trusted users considering the accepted chain length
	trustedUsers = new Set();
	// one trusts oneself -- address should always be lowercased if it could be compared
	trustedUsers.add(web3.eth.defaultAccount.toLowerCase());
	let trustLevel = (await browser.storage.local.get('trustLevel')).trustLevel;
	let trustedChainLength = (await browser.storage.local.get('trustedChainLength')).trustedChainLength;
	console.log(trustLevel);

	// Contains users whose trust relationships have been considered
	let seenRelationships = new Set();
	// Loop through trusted users and their trust relationships
	for (let i = 0; i < trustedChainLength; ++i) {
		let relationshipsOnCurrentDistance = new Set();
		for (let user of trustedUsers) {
			if (seenRelationships.has(user)) {
				continue;
			}
			let trusts = await tkiInstance.getTrustedUsers.call(user, trustLevel);
			seenRelationships.add(user);
			for (let trusted of trusts) {
				relationshipsOnCurrentDistance.add(trusted);
			}
		}
		for (let user of relationshipsOnCurrentDistance) {
			trustedUsers.add(user);
		}
	}
}