'use-strict';

let background = browser.extension.getBackgroundPage();

// Update system settings
document.getElementById('save').onclick = async function(err) {
	browser.storage.local.set({
		trustedChainLength: document.getElementById('trust-chain-length').value,
		trustLevel: document.getElementById('trust-level').value,
		afterBlock: document.getElementById('after-block').value,
	});
	let oldStyle = document.getElementById('save').style;
	document.getElementById('save').style.color = '#4CAF50';
	document.getElementById('save').style.fontWeight = 'bold';
	setTimeout(function() {
		document.getElementById('').style = oldStyle;
	}, 2000);
}

document.addEventListener('DOMContentLoaded', async function(event) {
	document.getElementById('trust-chain-length').value = (await browser.storage.local.get('trustedChainLength')).trustedChainLength;
	document.getElementById('trust-level').value = (await browser.storage.local.get('trustLevel')).trustLevel;
	document.getElementById('after-block').value = (await browser.storage.local.get('afterBlock')).afterBlock;

	// lowest block number cannot be greater than the current block number
	background.web3.eth.getBlockNumber(function(err, res) {
		document.getElementById('after-block').setAttribute('max', res);
	});
});
