'use-strict';

let background = browser.extension.getBackgroundPage();
let domainInPopup;


browser.storage.local.get().then(function(inStorage) {
	document.getElementById('send-votes').checked = inStorage.sendsVotes;
});

document.getElementById('send-votes').addEventListener('click', function(err) {
	browser.storage.local.set({
		sendsVotes: document.getElementById('send-votes').checked,
	});
});

document.getElementById('register').onclick = function(err) {
  	background.tkiInstance.register(background.web3.fromAscii("ThroughBrowserExtension"), [], {gas: 2000000});
}

document.getElementById('post-bindings').onclick = function(err) {
	background.postBindings();
}

document.getElementById('update-trust').onclick = async function(err) {
	await background.updateTrustedUsers();
	let oldStyle = document.getElementById('update-trust').style;
	document.getElementById('update-trust').style.color = '#4CAF50';
	document.getElementById('update-trust').style.fontWeight = 'bold';
	setTimeout(function() {
		document.getElementById('update-trust').style = oldStyle;
	}, 2000);
}

document.getElementById('options').onclick = function(err) {
	browser.tabs.create({url: 'options.html'});
}

// if user is not registered show 'register' button
background.tkiInstance.users(background.web3.eth.defaultAccount).then(function(user) {
	let registered = user[1];
	if (!registered) {
		document.getElementById('register').style.display = 'block';
	}
});

// if user encountered bindings that have not been posted to blockchain, show 'Post Bindings' button
if (Object.keys(background.bindingsToPostInSession).length > 0) {
	document.getElementById('post-bindings').style.display = 'block';
}

// set domain visits
for (let domain in background.sessionDomainsSeen) {

	let domainContainer = document.createElement('div');
	domainContainer.setAttribute('class', 'domain-container');

	let visitContainer = document.createElement('div');
	visitContainer.setAttribute('class', 'visit-container');

	let domainTitle = document.createElement('div');
	domainTitle.setAttribute('class', 'domain-title');
	let domainName = document.createElement('p');
	domainName.setAttribute('class', 'domain-name');
	// Take domain key from the sessionDomainsSeen mapping because some 
	// certificates cannot be processed
	domainName.innerHTML = domain;
	let shortInfo = document.createElement('p');
	shortInfo.setAttribute('class', 'short-info');
	shortInfo.innerHTML += '<span>' + background.sessionDomainsSeen[domain].allDomVisits + '</span>';
	shortInfo.innerHTML += '<span>' + background.sessionDomainsSeen[domain].trustedDomVisits + '</span>';
	shortInfo.innerHTML += '<span>' + background.sessionDomainsSeen[domain].allBindingVisits + '</span>';
	shortInfo.innerHTML += '<span>' + background.sessionDomainsSeen[domain].trustedBindingVisits + '</span>';
	domainTitle.appendChild(domainName);
	domainTitle.appendChild(shortInfo);

	let pubKeyHash = document.createElement('p');
	pubKeyHash.setAttribute('class', 'pub-key-hash');
	pubKeyHash.innerHTML = '<b>Public key hash encountered</b><br />' + background.sessionDomainsSeen[domain].keyHash;

	visitContainer.appendChild(pubKeyHash);

	let ulVisit = document.createElement('ul');
	ulVisit.setAttribute('class', 'visits');
	ulVisit.innerHTML += '<li>Visit Reward: </li>' + '<li>' +  background.sessionDomainsSeen[domain].sumOfVisitRewards + '</li>';
	ulVisit.innerHTML += '<li>Verification Reward: </li>' + '<li>' +  background.sessionDomainsSeen[domain].sumOfVerifRewards + '</li>';
	ulVisit.innerHTML += '<li>Reward Balance: </li>' + '<li>' +  background.sessionDomainsSeen[domain].sumOfRewardBalances + '</li>';
	ulVisit.innerHTML += '<li>Domain Visits: </li>' + '<li>' +  background.sessionDomainsSeen[domain].allDomVisits + '</li>';
	ulVisit.innerHTML += '<li>Trusted Domain Visits: </li>' + '<li>' +  background.sessionDomainsSeen[domain].trustedDomVisits + '</li>';
	ulVisit.innerHTML += '<li>Binding Encounters: </li>' + '<li>' +  background.sessionDomainsSeen[domain].allBindingVisits + '</li>';
	ulVisit.innerHTML += '<li>Trusted Encounters: </li>' + '<li>' +  background.sessionDomainsSeen[domain].trustedBindingVisits + '</li>';
	ulVisit.innerHTML += '<li>All Verifications: </li>' + '<li>' + background.sessionDomainsSeen[domain].allVerifications + '</li>';
	ulVisit.innerHTML += '<li>All Trusted Verifications: </li>' + '<li>' +  background.sessionDomainsSeen[domain].trustedVerifications + '</li>';

	// visitContainer.appendChild(domainTitle);
	visitContainer.appendChild(ulVisit);

	let claimContainer = document.createElement('div');
	claimContainer.setAttribute('class', 'claim-container');

	let strongestClaim = document.createElement('h3');
	strongestClaim.innerHTML = 'Strongest Claim';

	let ulVerif;
	ulVerif = document.createElement('ul');
	ulVerif.setAttribute('class', 'verifications');

	ulVerif.innerHTML += '<li>Claimant: </li>' + '<li class="address">' +  background.sessionDomainsSeen[domain].strongestClaimant + '</li>';
	ulVerif.innerHTML += '<li>Verifications: </li>' + '<li>' +  background.sessionDomainsSeen[domain].verifStrongestClaimant + '</li>';
	ulVerif.innerHTML += '<li>Trusted Verifications: </li>' + '<li>' +  background.sessionDomainsSeen[domain].trustedVerifStrongestClaimant + '</li>';

	claimContainer.appendChild(strongestClaim);
	claimContainer.appendChild(ulVerif);

	visitContainer.style.display = 'none';
	claimContainer.style.display = 'none';

	domainContainer.appendChild(domainTitle);
	domainContainer.appendChild(visitContainer);
	domainContainer.appendChild(claimContainer);

	document.body.appendChild(domainContainer);

	domainTitle.onclick = function(err) {
		if (visitContainer.style.display === 'none') {
			visitContainer.style.display = 'block';
			claimContainer.style.display = 'block';
		} else {
			visitContainer.style.display = 'none';
			claimContainer.style.display = 'none';
		}
	};
}